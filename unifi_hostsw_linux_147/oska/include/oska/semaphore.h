/*
 * Operating system kernel abstraction -- semaphores
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef __OSKA_SEMAPHORE_H
#define __OSKA_SEMAPHORE_H

#ifdef __OSKA_API_DOC
/**
 * @defgroup semaphore Semaphores
 */

/**
 * Implementation defined semaphore object.
 */
typedef implementation_defined os_semaphore_t;

/**
 * Initialize a semaphore to zero.
 *
 * Callable from: thread context.
 *
 * @param sem semaphore to initialize.
 *
 * @ingroup semaphore
 */
void os_semaphore_init(os_semaphore_t *sem);

/**
 * Wait for an event to be posted to the semaphore.
 *
 * Callable from: thread context.
 *
 * @param sem semaphore to wait on.
 *
 * @ingroup semaphore
 */
void os_semaphore_wait(os_semaphore_t *sem);

/**
 * Post an event to a semaphore, waking any threads waiting on it.
 *
 * Callable from: any context.
 *
 * @param sem semaphore to post to.
 *
 * @ingroup semaphore
 */
void os_semaphore_post(os_semaphore_t *sem);

#endif /* __OSKA_API_DOC */

#ifdef linux
#  include <../linux/semaphore.h>
#else
#  error <oska/semaphore.h> not provided for this OS
#endif

#endif /* #ifndef __OSKA_SEMAPHORE_H */
