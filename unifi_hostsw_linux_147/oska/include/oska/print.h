/*
 * Operating system kernel abstraction -- console printing
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef __OSKA_PRINT_H
#define __OSKA_PRINT_H

#ifdef __OSKA_API_DOC
/**
 * @defgroup print Console or log output
 */

/**
 * Write a text message to a log or the console.
 *
 * The c, d, p, s, and x conversion specifiers and the field length
 * and pad character should be supported (e.g., \%08x).
 *
 * The message may be truncated if it's longer than 80 characters.
 *
 * Callable from: any context.
 *
 * @param level  severity of the message.
 * @param prefix string to be prefixed to the message.
 * @param name   the name of the device or subsystem producing the
 *               message (may be NULL).
 * @param format printf-style format string.
 * @param ...    arguments for \a format.
 *
 * @ingroup print
 */
void os_print(enum os_print_level level, const char *prefix, const char *name,
              const char *format, ...);

/**
 * Write a text message to a log or the console.
 *
 * A variant of os_print() accepting a va_list for the \a format
 * arguments.
 *
 * Callable from: any context.
 *
 * @param level  severity of the message.
 * @param prefix string to be prefixed to the message.
 * @param name   the name of the device or subsystem producing the
 *               message (may be NULL).
 * @param format printf-style format string.
 * @param args   arguments for \a format.
 *
 * @ingroup print
 */
void os_vprint(enum os_print_level level, const char *prefix, const char *name,
               const char *format, va_list args);

#endif /* __OSKA_API_DOC */

/**
 * Severity of a console or log message.
 *
 * @ingroup print
 */
enum os_print_level {
    OS_PRINT_ERROR,
    OS_PRINT_WARNING,
    OS_PRINT_INFO,
    OS_PRINT_DEBUG,
};

#ifdef linux
#  include <../linux/print.h>
#else
#  error <oska/print.h> not provided for this OS
#endif

#endif /* #ifndef __OSKA_PRINT_H */
