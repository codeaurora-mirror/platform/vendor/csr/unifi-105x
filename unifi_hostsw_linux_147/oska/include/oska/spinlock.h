/*
 * Operating system kernel abstraction -- spinlocks
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef __OSKA_SPINLOCK_H
#define __OSKA_SPINLOCK_H

#ifdef __OSKA_API_DOC
/**
 * @defgroup spinlock Spinlocks
 */

/**
 * Implementation defined spinlock object.
 *
 * @ingroup spinlock
 */
typedef implementation_defined os_spinlock_t;

/**
 * Initialize a spinlock.
 *
 * Callable from: thread context.
 *
 * @param lock pointer to the spinlock to initialize.
 *
 * @ingroup spinlock
 */
void os_spinlock_init(os_spinlock_t *lock);

/**
 * Destroy a spinlock, freeing any resources the OS may have allocated.
 *
 * Callable from: thread context.
 *
 * @param lock pointer to the spinlock to destroy.
 *
 * @ingroup spinlock
 */
void os_spinlock_destroy(os_spinlock_t *lock);

/**
 * Lock a spinlock and disable interrupts (after saving the interrupt
 * state).
 *
 * Callable from: any context.
 *
 * @param lock      pointer to the spinlock to lock.
 * @param int_state for saved interrupt state.
 *
 * @ingroup spinlock
 */
void os_spinlock_lock_intsave(os_spinlock_t *lock, os_int_status_t *int_state);

/**
 * Unlock a spinlock, restoring the prior interrupt state.
 *
 * Callable from: any context.
 * 
 * @param lock      spinlock to unlock.
 * @param int_state interrupt state to restore.
 *
 * @ingroup spinlock
 */
void os_spinlock_unlock_intrestore(os_spinlock_t *lock, os_int_status_t *int_state);

#endif /* __OSKA_API_DOC */

#ifdef linux
#  include <../linux/spinlock.h>
#else
#  error <oska/spinlock.h> not provided for this OS
#endif

#endif /* #ifndef __OSKA_SPINLOCK_H */
