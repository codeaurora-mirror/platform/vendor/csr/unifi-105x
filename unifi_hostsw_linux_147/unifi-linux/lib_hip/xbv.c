/*
 * ---------------------------------------------------------------------------
 * FILE: download.c
 *
 * PURPOSE:
 *      Routines for downloading firmware to UniFi.
 *      
 *      UniFi firmware files use a nested TLV (Tag-Length-Value) format.
 *
 * Copyright (C) 2005-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */
#ifdef TEST
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <limits.h>
#include "driver/signals.h"
#include "unifihw.h"
void unifi_error(void* ospriv, const char *fmt, ...);
struct card {
    void *ospriv;
};
typedef struct card card_t;
#else
#include "driver/unifiversion.h"
#include "card.h"
#endif /* TEST */
#include "xbv.h"


/* Struct to represent a buffer for reading firmware file */

typedef struct {
    void *dlpriv;
    int ioffset;
    fwreadfn_t iread;
} ct_t;

/* Struct to represent a TLV field */
typedef struct {
    char t_name[4];
    int  t_len;
} tag_t;


#define TAG_EQ(i, v)    (((i)[0] == (v)[0]) &&  \
                         ((i)[1] == (v)[1]) &&  \
                         ((i)[2] == (v)[2]) &&  \
                         ((i)[3] == (v)[3]))

/* We create a small stack on the stack that contains an enum
 * indicating the containing list segments, and the offset at which
 * those lists end.  This enables a lot more error checking. */
typedef enum
{
    xbv_xbv1,
  /*xbv_info,*/
    xbv_fw,
    xbv_vers,
    xbv_vand,
    xbv_ptch,
    xbv_other
} xbv_container;

#define XBV_STACK_SIZE 6

typedef struct
{
    struct
    {
        xbv_container container;
        int ioffset_end;
    } s[XBV_STACK_SIZE];
    int ptr;
} xbv_stack_t;

static int read_tag(card_t *card, ct_t *ct, tag_t *tag);
static int read_bytes(card_t *card, ct_t *ct, void *buf, int len);
static int read_uint(card_t *card, ct_t *ct, unsigned int *u, int len);
static int xbv_check(xbv1_t *fwinfo, const xbv_stack_t *stack,
                     xbv_mode new_mode, xbv_container old_cont);
static int xbv_push(xbv1_t *fwinfo, xbv_stack_t *stack,
                    xbv_mode new_mode, xbv_container old_cont,
                    xbv_container new_cont, int ioff);


/*
 * ---------------------------------------------------------------------------
 *  parse_xbv1
 *
 *      Scan the firmware file to find the TLVs we are interested in.
 *      Actions performed:
 *        - check we support the file format version in VERF
 *      Store these TLVs if we have a firmware image:
 *        - SLTP Symbol Lookup Table Pointer
 *        - FWDL firmware download segments
 *        - FWOL firmware overlay segment
 *        - VMEQ Register probe tests to verify matching h/w
 *      Store these TLVs if we have a patch file:
 *        - FWID the firmware build ID that this file patches
 *        - PTDL The actual patches
 *
 *      The structure pointed to by fwinfo is cleared and
 *      'fwinfo->mode' is set to 'unknown'.  The 'fwinfo->mode'
 *      variable is set to 'firmware' or 'patch' once we know which
 *      sort of XBV file we have.
 * 
 *  Arguments:
 *      readfn          Pointer to function to call to read from the file.
 *      dlpriv          Opaque pointer arg to pass to readfn.
 *      fwinfo          Pointer to fwinfo struct to fill in.
 *
 *  Returns:
 *      0 on success, -1 on error.
 * ---------------------------------------------------------------------------
 */
int
xbv1_parse(card_t *card, fwreadfn_t readfn, void *dlpriv, xbv1_t *fwinfo)
{
    ct_t ct;
    tag_t tag;
    xbv_stack_t stack;

    ct.dlpriv = dlpriv;
    ct.ioffset = 0;
    ct.iread = readfn;

    memset(fwinfo, 0, sizeof(xbv1_t));
    fwinfo->mode = xbv_unknown;

    /* File must start with XBV1 triplet */
    if (read_tag(card, &ct, &tag) <= 0) {
        return -1;
    }

    if (!TAG_EQ(tag.t_name, "XBV1")) {
        unifi_error(NULL, "File is not UniFi firmware\n");
        return -1;
    }

    stack.ptr = 0;
    stack.s[stack.ptr].container = xbv_xbv1;
    stack.s[stack.ptr].ioffset_end = INT_MAX;

    /* Now scan the file */
    while (1) {
        int n;

        n = read_tag(card, &ct, &tag);
        if (n < 0) {
            return -1;
        }
        if (n == 0) {
            /* End of file */
            break;
        }

        /* File format version */
        if (TAG_EQ(tag.t_name, "VERF")) {
            unsigned int version;

            if (xbv_check(fwinfo, &stack, xbv_unknown, xbv_xbv1) ||
                (tag.t_len != 2) ||
                read_uint(card, &ct, &version, 2)) {
                return -1;
            }
            if (version != 0) {
                unifi_error(NULL, "Unsupported firmware file version: %d.%d\n",
                            version >> 8, version & 0xFF);
                return -1;
            }                
        }
        else if (TAG_EQ(tag.t_name, "LIST"))
        {
            char name[4];
            int list_end;

            list_end = ct.ioffset + tag.t_len;

            if (read_bytes(card, &ct, name, 4))
                return -1;

            if (TAG_EQ(name, "FW  "))
            {
                if (xbv_push(fwinfo, &stack, xbv_firmware, xbv_xbv1, xbv_fw, list_end))
                    return -1;
            }
            else if (TAG_EQ(name, "VERS"))
            {
                if (xbv_push(fwinfo, &stack, xbv_firmware, xbv_fw, xbv_vers, list_end) ||
                    (fwinfo->vers.num_vand != 0))
                    return -1;
            }
            else if (TAG_EQ(name, "VAND"))
            {
                struct VAND *vand;

                if (xbv_push(fwinfo, &stack, xbv_firmware, xbv_vers, xbv_vand, list_end) ||
                    (fwinfo->vers.num_vand >= MAX_VAND))
                    return -1;

                /* Get a new VAND */
                vand = fwinfo->vand + fwinfo->vers.num_vand++;

                /* Fill it in */
                vand->first = fwinfo->num_vmeq;
                vand->count = 0;
            }
            else if (TAG_EQ(name, "PTCH"))
            {
                if (xbv_push(fwinfo, &stack, xbv_patch, xbv_xbv1, xbv_ptch, list_end))
                    return -1;
            }
            else
            {
                /* Skip over any other lists.  We dont bother to push
                 * the new list type now as we would only pop it at
                 * the end of the outer loop. */
                ct.ioffset += tag.t_len - 4;
            }
        }
        else if (TAG_EQ(tag.t_name, "SLTP"))
        {
            unsigned int addr;

            if (xbv_check(fwinfo, &stack, xbv_firmware, xbv_fw) ||
                (tag.t_len != 4) ||
                (fwinfo->slut_addr != 0) ||
                read_uint(card, &ct, &addr, 4)) {
                return -1;
            }

            fwinfo->slut_addr = addr;
        }
        else if (TAG_EQ(tag.t_name, "FWDL"))
        {
            unsigned int addr;
            struct FWDL *fwdl;

            if (xbv_check(fwinfo, &stack, xbv_firmware, xbv_fw) ||
                (fwinfo->num_fwdl >= MAX_FWDL) ||
                (read_uint(card, &ct, &addr, 4))) {
                return -1;
            }

            fwdl = fwinfo->fwdl + fwinfo->num_fwdl++;

            fwdl->dl_size = tag.t_len - 4;
            fwdl->dl_addr = addr;
            fwdl->dl_offset = ct.ioffset;

            ct.ioffset += tag.t_len - 4;
        }
        else if (TAG_EQ(tag.t_name, "FWOV"))
        {
            if (xbv_check(fwinfo, &stack, xbv_firmware, xbv_fw) ||
                (fwinfo->fwov.dl_size != 0) ||
                (fwinfo->fwov.dl_offset != 0)) {
                return -1;
            }

            fwinfo->fwov.dl_size = tag.t_len;
            fwinfo->fwov.dl_offset = ct.ioffset;

            ct.ioffset += tag.t_len;
        }
        else if (TAG_EQ(tag.t_name, "VMEQ"))
        {
            unsigned int temp[3];
            struct VAND *vand;
            struct VMEQ *vmeq;

            if (xbv_check(fwinfo, &stack, xbv_firmware, xbv_vand) ||
                (fwinfo->num_vmeq >= MAX_VMEQ) ||
                (fwinfo->vers.num_vand == 0) ||
                (tag.t_len != 8) ||
                read_uint(card, &ct, &temp[0], 4) ||
                read_uint(card, &ct, &temp[1], 2) ||
                read_uint(card, &ct, &temp[2], 2)) {
                return -1;
            }

            /* Get the last VAND */
            vand = fwinfo->vand + (fwinfo->vers.num_vand - 1);

            /* Get a new VMEQ */
            vmeq = fwinfo->vmeq + fwinfo->num_vmeq++;

            /* Note that this VAND contains another VMEQ */
            vand->count++;

            /* Fill in the VMEQ */
            vmeq->addr = temp[0];
            vmeq->mask = (uint16)temp[1];
            vmeq->value = (uint16)temp[2];
        }
        else if (TAG_EQ(tag.t_name, "FWID"))
        {
            unsigned int build_id;

            if (xbv_check(fwinfo, &stack, xbv_patch, xbv_ptch) ||
                (tag.t_len != 4) ||
                (fwinfo->build_id != 0) ||
                read_uint(card, &ct, &build_id, 4))
                return -1;

            fwinfo->build_id = build_id;
        }
        else if (TAG_EQ(tag.t_name, "PTDL"))
        {
            struct PTDL *ptdl;

            if (xbv_check(fwinfo, &stack, xbv_patch, xbv_ptch) ||
                (fwinfo->num_ptdl >= MAX_PTDL))
                return -1;

            /* Allocate a new PTDL */
            ptdl = fwinfo->ptdl + fwinfo->num_ptdl++;

            ptdl->dl_size = tag.t_len;
            ptdl->dl_offset = ct.ioffset;

            ct.ioffset += tag.t_len;
        }
        else
        {
            /* 
             * If we get here it is a tag we are not interested in, 
             * just skip over it.
             */
            ct.ioffset += tag.t_len;
        }

        /* Check to see if we are at the end of the currently stacked
         * segment.  We could finish more than one list at a time. */
        while (ct.ioffset >= stack.s[stack.ptr].ioffset_end) {
            if (ct.ioffset > stack.s[stack.ptr].ioffset_end) {
                unifi_error(NULL, "XBV file has overrun stack'd segment\n");
                return -1;
            }
            if (stack.ptr <= 0) {
                unifi_error(NULL, "XBV file has underrun stack pointer\n");
                return -1;
            }
            stack.ptr--;
        }
    }

    if (stack.ptr != 0) {
        unifi_error(NULL, "Last list of XBV is not complete.\n");
        return -1;
    }

    return 0;
} /* xbv1_parse() */


/* Check the the XBV file is of a consistant sort (either firmware or
 * patch) and that we are in the correct containing list type. */
static int xbv_check(xbv1_t *fwinfo, const xbv_stack_t *stack,
                     xbv_mode new_mode, xbv_container old_cont)
{
    /* If the new file mode is unknown the current packet could be in
     * either (any) type of XBV file, and we cant make a decission at
     * this time. */
    if (new_mode != xbv_unknown) {
        if (fwinfo->mode == xbv_unknown) {
            fwinfo->mode = new_mode;
        } else if (fwinfo->mode != new_mode) {
            return -1;
        }
    }
    /* If the current stack top doesn't match what we expect then the
     * file is corrupt. */
    if (stack->s[stack->ptr].container != old_cont) {
        return -1;
    }
    return 0;
}

/* Make checks as above and then enter a new list */
static int xbv_push(xbv1_t *fwinfo, xbv_stack_t *stack,
                    xbv_mode new_mode, xbv_container old_cont,
                    xbv_container new_cont, int new_ioff)
{
    if (xbv_check(fwinfo, stack, new_mode, old_cont)) {
        return -1;
    }

    /* Check that our stack won't overflow. */
    if (stack->ptr >= (XBV_STACK_SIZE - 1))
        return -1;

    /* Add the new list element to the top of the stack. */
    stack->ptr++;
    stack->s[stack->ptr].container = new_cont;
    stack->s[stack->ptr].ioffset_end = new_ioff;

    return 0;
}


static unsigned int
xbv2uint(unsigned char *ptr, int len)
{
    unsigned int u = 0;
    int i;

    for (i = 0; i < len; i++) {
        unsigned int b;
        b = ptr[i];
        u += b << (i * 8);
    }
    return u;
}



static int
read_tag(card_t *card, ct_t *ct, tag_t *tag)
{
    unsigned char buf[8];
    int n;

    n = (*ct->iread)(card->ospriv, ct->dlpriv, ct->ioffset, buf, 8);
    if (n <= 0) {
        return n;
    }

    /* read the tag and length */
    if (n != 8) {
        return -1;
    }

    /* get section tag */
    memcpy(tag->t_name, buf, 4);

    /* get section length */
    tag->t_len = xbv2uint(buf+4, 4);

    ct->ioffset += 8;

    return 8;
} /* read_tag() */


static int
read_bytes(card_t *card, ct_t *ct, void *buf, int len)
{
    /* read the tag value */
    if ((*ct->iread)(card->ospriv, ct->dlpriv, ct->ioffset, buf, len) != len) {
        return -1;
    }

    ct->ioffset += len;

    return 0;
} /* read_bytes() */

static int
read_uint(card_t *card, ct_t *ct, unsigned int *u, int len)
{
    unsigned char buf[4];

    /* Integer cannot be more than 4 bytes */
    if (len > 4) {
        return -1;
    }

    if (read_bytes(card, ct, buf, len)) return -1;

    *u = xbv2uint(buf, len);

    return 0;
} /* read_uint() */


/*
 * ---------------------------------------------------------------------------
 *  read_slut
 *
 *      desc
 * 
 *  Arguments:
 *      readfn          Pointer to function to call to read from the file.
 *      dlpriv          Opaque pointer arg to pass to readfn.
 *      addr            Offset into firmware image of SLUT.
 *      fwinfo          Pointer to fwinfo struct to fill in.
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
int
xbv1_read_slut(card_t *card, fwreadfn_t readfn, void *dlpriv, xbv1_t *fwinfo,
               symbol_t *slut, int slut_len)
{
    int i;
    int offset;
    unsigned int magic;
    int count = 0;
    ct_t ct;

    if (fwinfo->mode != xbv_firmware) {
        return -1;
    }

    /* Find the d/l segment containing the SLUT */
    /* This relies on the SLUT being entirely contained in one segment */
    offset = -1;
    for (i = 0; i < fwinfo->num_fwdl; i++)
    {
        if ((fwinfo->slut_addr >= fwinfo->fwdl[i].dl_addr) &&
            (fwinfo->slut_addr < (fwinfo->fwdl[i].dl_addr + fwinfo->fwdl[i].dl_size)))
        {
            offset = fwinfo->fwdl[i].dl_offset +
                (fwinfo->slut_addr - fwinfo->fwdl[i].dl_addr);
        }
    }
    if (offset < 0) {
        return -1;
    }

    ct.dlpriv = dlpriv;
    ct.ioffset = offset;
    ct.iread = readfn;

    if (read_uint(card, &ct, &magic, 2)) return -1;
    if (magic != SLUT_FINGERPRINT) {
        return -1;
    }

    while (count < slut_len) {
        unsigned int id, obj;

        /* Read Symbol Id */
        if (read_uint(card, &ct, &id, 2)) return -1;

        /* Check for end of table marker */
        if (id == CSR_SLT_END) {
            break;
        }

        /* Read Symbol Value */
        if (read_uint(card, &ct, &obj, 4)) return -1;

        slut[count].id  = (uint16)id;
        slut[count].obj = obj;
        count++;
    }

    return count;

} /* read_slut() */


#ifdef TEST
/*
 * ---------------------------------------------------------------------------
 *
 * This file will compile with -DTEST.
 * The resulting executable will scan and print out the f/w info derived from
 * test.xbv.
 *
 * ---------------------------------------------------------------------------
 */
struct img {
    unsigned char *imgptr;
    int imglen;
};

static int
rfn(void* ospriv, void *dlpriv, int offset, void *buf, int len)
{
    struct img *img = dlpriv;


    if (offset == img->imglen) {
        /* at end of file */
        return 0;
    }

    if ((offset+len) > img->imglen) {
        /* attempt to read past end of file */
        return -1;
    }

    memcpy(buf, img->imgptr+offset, len);

    return len;
}

void unifi_error(void* ospriv, const char *fmt, ...)
{
    va_list ap;

    va_start(ap, fmt);
    vprintf(fmt, ap);
    va_end(ap);
}

int
main(int argc, char *argv[])
{
    char *filename = "test.xbv";
    FILE *fp;
    unsigned char *buf;
    int i, j, n;
    ct_t ct;
    struct img img;
    xbv1_t fwinfo;
#define MAX_SLUT_ENTRIES 32
    symbol_t slut[MAX_SLUT_ENTRIES];
    int num_slut_entries;
    struct card card;

    if (argc > 1) {
        filename = argv[1];
    }

    buf = malloc(256*1024);

    fp = fopen(filename, "r");
    if (!fp) {
        perror("Failed to open test.xbv");
        exit(errno);
    }
    n = fread(buf, 1, 256*1024, fp);
    if (n < 0) {
        perror("Failed to read test.xbv");
        exit(errno);
    }


    memset(&fwinfo, 0, sizeof(fwinfo));

    img.imgptr = buf;
    img.imglen = n;

    n = xbv1_parse(&card, rfn, &img, &fwinfo);
    if (n) {
        printf("Parse failed\n");
        exit(1);
    }

    switch (fwinfo.mode)
    {
    case xbv_firmware:
        printf("XBV File contains a firmware image.\n");
        num_slut_entries = xbv1_read_slut(&card, rfn, &img, &fwinfo, slut, MAX_SLUT_ENTRIES);

        if (fwinfo.vers.num_vand != 0) {
            printf("    VERS: for %d\n", fwinfo.vers.num_vand);

            for (i = 0; i < fwinfo.vers.num_vand; i++) {
                printf("      VAND: %d for %d\n",
                       fwinfo.vand[i].first, fwinfo.vand[i].count);

                for (j = 0; j < fwinfo.vand[i].count; j++) {
                    int k = fwinfo.vand[i].first + j;
                    printf("        VMEQ: *0x%08lx & 0x%04X == 0x%04X\n",
                           fwinfo.vmeq[k].addr, fwinfo.vmeq[k].mask, fwinfo.vmeq[k].value);
                }
            }
        }

        printf("SLUT addr 0x%X\n", fwinfo.slut_addr);
        printf("%d SLUT entr%s:\n", num_slut_entries, (num_slut_entries == 1) ? "y" : "ies");
        for (i = 0; i < num_slut_entries; i++) {
            printf("    SLUT: 0x%04X, 0x%08X\n",
                   slut[i].id, slut[i].obj);
        }

        printf("%d F/W segment%s\n", fwinfo.num_fwdl, (fwinfo.num_fwdl == 1) ? "" : "s");
        for (i = 0; i < fwinfo.num_fwdl; i++) {
            printf("    FWDL: dest 0x%08lx, len %6d, file offset 0x%X\n",
                   fwinfo.fwdl[i].dl_addr, fwinfo.fwdl[i].dl_size,
                   fwinfo.fwdl[i].dl_offset);
        }

        if (fwinfo.fwov.dl_size > 0) {
            printf("Overlay segment\n");
            printf("    FWOV: len %6d, file offset 0x%X\n",
                   fwinfo.fwov.dl_size, fwinfo.fwov.dl_offset);
        } else {
            printf("No overlay segment\n");
        }
        break;

    case xbv_patch:
        printf("XBV File contains a patch set.\n");

        printf("FWID build id %u\n", fwinfo.build_id);

        printf("%d Patch segment%s\n", fwinfo.num_ptdl, (fwinfo.num_ptdl == 1) ? "" : "s");
        for (i = 0; i < fwinfo.num_ptdl; i++) {
            printf("    PTDL: len %5d, file offset 0x%X\n",
                   fwinfo.ptdl[i].dl_size, fwinfo.ptdl[i].dl_offset);
        }
        break;

    default:
        printf("XBV File seems to be corrupt.\n");
        break;
    }
}

#endif
