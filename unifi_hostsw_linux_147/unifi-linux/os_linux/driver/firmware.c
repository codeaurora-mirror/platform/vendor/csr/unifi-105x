/*
 * ---------------------------------------------------------------------------
 *  FILE:     firmware.c
 * 
 *  PURPOSE:
 *      Example code for performing firmware download to UniFi.
 *
 * Copyright (C) 2005-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */
#include <linux/kmod.h>
#include <linux/vmalloc.h>
#include <linux/firmware.h>
#include <asm/uaccess.h>
#include "driver/unifi.h"
#include "driver/unifi_udi.h"
#include "unifiio.h"
#include "unifi_priv.h"


#define UNIFIHELPER_INIT_MODE_SMEEMB    0
#define UNIFIHELPER_INIT_MODE_SMEUSER   2
#define UNIFIHELPER_INIT_MODE_NATIVE    1

/*
 * ---------------------------------------------------------------------------
 *  uf_run_unifihelper
 *
 *      Ask userspace to send us firmware for download by running
 *      '/usr/sbin/unififw'.
 *      Derived from net_run_sbin_hotplug().
 * 
 *  Arguments:
 *      priv            Pointer to OS private struct.
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
int
uf_run_unifihelper(unifi_priv_t *priv)
{
#ifdef CONFIG_HOTPLUG

#ifdef ANDROID_BUILD
    char *prog = "/system/bin/unififw";
#else
    char *prog = "/usr/sbin/unififw";
#endif /* ANDROID_BUILD */

    char *argv[6], *envp[4];
    char inst_str[8];
    char init_mode[8];
    int i, r;

#if (defined CSR_SME_USERSPACE) && (!defined CSR_SUPPORT_WEXT)
    unifi_trace(priv, UDBG1, "SME userspace build: run unifi_helper manually\n");
    return 0;
#endif

    unifi_trace(priv, UDBG1, "starting %s\n", prog);

    snprintf(inst_str,   8, "%d", priv->instance);
#if (defined CSR_SME_EMB)
    snprintf(init_mode, 8, "%d", UNIFIHELPER_INIT_MODE_SMEEMB);
#elif (defined CSR_SME_USERSPACE)
    snprintf(init_mode, 8, "%d", UNIFIHELPER_INIT_MODE_SMEUSER);
#else
    snprintf(init_mode, 8, "%d", UNIFIHELPER_INIT_MODE_NATIVE);
#endif /* CSR_SME_EMB */

    i = 0;
    argv[i++] = prog;
    argv[i++] = inst_str;
    argv[i++] = init_mode;
    argv[i++] = 0;
    argv[i] = 0;
    /* Don't add more args without making argv bigger */

    /* minimal command environment */
    i = 0;
    envp[i++] = "HOME=/";
    envp[i++] = "PATH=/sbin:/bin:/usr/sbin:/usr/bin";
    envp[i] = 0;
    /* Don't add more without making envp bigger */

    unifi_trace(priv, UDBG2, "running %s %s %s\n", argv[0], argv[1], argv[2]);

    r = call_usermodehelper(argv[0], argv, envp, 0);

    return r;
#else
    unifi_trace(priv, UDBG1, "Can't automatically download firmware because kernel does not have HOTPLUG\n");
    return -1;
#endif
} /* uf_run_unifihelper() */



/*
 * ---------------------------------------------------------------------------
 *  uf_request_firmware_files
 *
 *      Get the firmware files from userspace.
 * 
 *  Arguments:
 *      priv            Pointer to OS private struct.
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
int uf_request_firmware_files(unifi_priv_t *priv)
{
    /* uses the default method to get the firmware */
    const struct firmware *fw_entry;
    int postfix;
#define UNIFI_MAX_FW_PATH_LEN       32
    char fw_name[UNIFI_MAX_FW_PATH_LEN];
    int r;

    /*
     * The buffers for holding f/w images were allocated using vmalloc
     * so must be freed with vfree.
     */
    if (priv->fw_loader.dl_data) {
        vfree(priv->fw_loader.dl_data);
        priv->fw_loader.dl_data = NULL;
        priv->fw_loader.dl_len = 0;
    }
    if (priv->fw_sta.dl_data) {
        vfree(priv->fw_sta.dl_data);
        priv->fw_sta.dl_data = NULL;
        priv->fw_sta.dl_len = 0;
    }
#if (defined CSR_SUPPORT_SME) && (defined CSR_SUPPORT_WEXT)
    if (priv->mib_data.length) {
        vfree(priv->mib_data.data);
        priv->mib_data.data = NULL;
        priv->mib_data.length = 0;
    }
#endif /* CSR_SUPPORT_SME && CSR_SUPPORT_WEXT*/

    postfix = priv->instance;

    scnprintf(fw_name, UNIFI_MAX_FW_PATH_LEN, "unifi-sdio-%d/%s",
              postfix, "sta.xbv");
    r = request_firmware(&fw_entry, fw_name, priv->unifi_device);
    if (r == 0) {
        priv->fw_sta.dl_data = vmalloc(fw_entry->size);
        if (priv->fw_sta.dl_data == NULL)
        {
            unifi_error(priv,
                        "Failed to allocate memory for firmware image\n");
            return -ENOMEM;
        }

        memcpy(priv->fw_sta.dl_data, fw_entry->data, fw_entry->size);
        priv->fw_sta.dl_len = fw_entry->size;
        release_firmware(fw_entry);
    } else {
        unifi_trace(priv, UDBG2, "Firmware file not available\n");
    }

    scnprintf(fw_name, UNIFI_MAX_FW_PATH_LEN, "unifi-sdio-%d/%s",
              postfix, "loader.xbv");
    r = request_firmware(&fw_entry, fw_name, priv->unifi_device);
    if (r == 0) {
        priv->fw_loader.dl_data = vmalloc(fw_entry->size);
        if (priv->fw_loader.dl_data == NULL)
        {
            unifi_error(priv,
                        "Failed to allocate memory for firmware loader image\n");
            if (priv->fw_sta.dl_data) {
                vfree(priv->fw_sta.dl_data);
                priv->fw_sta.dl_data = NULL;
                priv->fw_sta.dl_len = 0;
            }
            return -ENOMEM;
        }

        memcpy(priv->fw_loader.dl_data, fw_entry->data, fw_entry->size);
        priv->fw_loader.dl_len = fw_entry->size;
        release_firmware(fw_entry);
    } else {
        unifi_trace(priv, UDBG2, "Loader file not available\n");
    }

#ifdef CSR_SME_EMB
    r = uf_request_mib_file(priv);
    if (r) {
        return r;
    }

    uf_request_mac_address_file(priv);
#endif /* CSR_SME_EMB */

    return 0;

} /* uf_request_firmware_files() */


