/** @file osa_support.h
 *
 * Operating System Abstraction support header file
 * 
 *   Copyright (C) Cambridge Silicon Radio Ltd 2006-2008. All rights reserved.
 * 
 * @section DESCRIPTION
 *   Provides the hooks down into the target OS that are needed by the OS
 *   abstraction layer.
 * 
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 ****************************************************************************/
#ifndef OSA_SUPPORT_H
#define OSA_SUPPORT_H

#include <linux/version.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/time.h>
#include <linux/list.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/netdevice.h>

#undef  osa_assert
#define osa_assert 

#undef osa_malloc
#undef osa_calloc
#undef osa_free
#undef osa_strtoul
#undef osa_sprintf
#define osa_malloc(sz) sme_osa_malloc(sz)
#define osa_calloc(nmemb, sz) sme_osa_calloc(nmemb, sz)
#define osa_free(ptr) sme_osa_free(ptr)
#define osa_strtoul(nptr, endptr, base) simple_strtoul(nptr, endptr, base)
#define osa_sprintf sprintf

void *sme_osa_malloc(unsigned int sz);
void sme_osa_free(void *ptr);
void *sme_osa_calloc(unsigned int nmemb, unsigned int sz);

#endif  /* OSA_SUPPORT_H */
