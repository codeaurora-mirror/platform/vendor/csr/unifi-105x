/*
 * ---------------------------------------------------------------------------
 *
 * FILE: sdio_emb.c
 *
 * PURPOSE: Driver instantiation and deletion for SDIO on Linux.
 *
 *      This file brings together the SDIO bus interface, the UniFi
 *      driver core and the Linux net_device stack.
 *
 * Copyright (C) 2007-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */
#include <linux/kmod.h>
#include <linux/init.h>
#include "driver/unifi.h"
#include "unifi_priv.h"

#include <sdioemb/sdio_api.h>


/* sdioemb driver uses POSIX error codes too */
#define convert_sdio_error(_r) (_r)



int
unifi_sdio_readb(void *sdio, int funcnum, unsigned long addr,
                 unsigned char *pdata)
{
    struct sdio_dev *fdev = sdio;
    int err;
    if (funcnum == 0) {
        err = sdio_f0_read8(fdev, addr, pdata);
    } else {
        err = sdio_read8(fdev, addr, pdata);
    }
    return convert_sdio_error(err);
} /* unifi_sdio_readb() */

int
unifi_sdio_writeb(void *sdio, int funcnum, unsigned long addr,
                  unsigned char data)
{
    struct sdio_dev *fdev = sdio;
    int err;
    if (funcnum == 0) {
        err = sdio_f0_write8(fdev, addr, data);
    } else {
        err = sdio_write8(fdev, addr, data);
    }
    return convert_sdio_error(err);
} /* unifi_sdio_writeb() */


int
unifi_sdio_block_rw(void *sdio, int funcnum, unsigned long addr,
                    unsigned char *pdata, unsigned int count, int direction)
{
    struct sdio_dev *fdev = sdio;
    int err;

    if (direction) {
        err = sdio_write(fdev, addr, pdata, count);
    } else {
        err = sdio_read(fdev, addr, pdata, count);
    }
    return convert_sdio_error(err);
} /* unifi_sdio_block_rw() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_set_max_clock_speed
 *
 *      Set the maximum SDIO bus clock speed to use.
 *
 *  Arguments:
 *      sdio            SDIO context pointer
 *      max_khz         maximum clock speed in kHz
 *
 *  Returns:
 *      Set clock speed in kHz; or a UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
int
unifi_sdio_set_max_clock_speed(void *sdio, int max_khz)
{
    struct sdio_dev *fdev = sdio;

    if (max_khz <= 0 || max_khz > sdio_clock) {
        max_khz = sdio_clock;
    }
    sdio_set_max_bus_freq(fdev, 1000 * max_khz);
    return max_khz;
} /* unifi_sdio_set_max_clock_speed() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_enable_interrupt
 *
 *      Enable or disable the SDIO interrupt.
 *      The driver can be made more efficient by disabling the SDIO interrupt
 *      during processing.
 *      The SDIO interrupt can be disabled by modifying the SDIO_INT_ENABLE
 *      register in the Card Common Control Register block, but this requires
 *      two CMD52 operations. A better solution is to mask the interrupt at
 *      the host controller.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 *      enable          If zero disable (or mask) the interrupt, otherwise
 *                      enable (or unmask) it.
 *
 *  Returns:
 *      Zero on success or a UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
int
unifi_sdio_enable_interrupt(void *sdio, int enable)
{
    struct sdio_dev *fdev = sdio;

    if (enable) {
        sdio_enable_interrupt(fdev);
    } else {
        sdio_disable_interrupt(fdev);
    }
    return 0;
} /* unifi_sdio_enable_interrupt() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_enable
 *
 *      Enable i/o on this function.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 *
 * Returns:
 *      UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
int
unifi_sdio_enable(void *sdio)
{
    struct sdio_dev *fdev = sdio;
    int r;

    /* Enable UniFi function (the 802.11 part). */
    r = sdio_enable_function(fdev);
    if (r) {
        unifi_error(NULL, "Failed to enable SDIO function %d\n", fdev->function);
        return convert_sdio_error(r);
    }
    return 0;
} /* unifi_sdio_enable() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_disable
 *
 *      Disable i/o on this function.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 *
 * Returns:
 *      UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
int
unifi_sdio_disable(void *sdio)
{
    struct sdio_dev *fdev = sdio;
    int r;

    /* Disable UniFi function (the 802.11 part). */
    r = sdio_disable_function(fdev);
    if (r) {
        unifi_error(NULL, "Failed to disable SDIO function %d\n", fdev->function);
        return convert_sdio_error(r);
    }
    return 0;
} /* unifi_sdio_disable() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_active
 *
 *      No-op as the bus goes to an active state at the start of every
 *      command.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 * ---------------------------------------------------------------------------
 */
void
unifi_sdio_active(void *sdio)
{
}

/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_idle
 *
 *      Set the function as idle.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 * ---------------------------------------------------------------------------
 */
void
unifi_sdio_idle(void *sdio)
{
    struct sdio_dev *fdev = sdio;

    sdio_idle_function(fdev);
} /* unifi_sdio_idle() */


int unifi_sdio_power_on(void *sdio)
{
    struct sdio_dev *fdev = sdio;

    sdio_power_on(fdev);
    return 0;
}

void unifi_sdio_power_off(void *sdio)
{
    struct sdio_dev *fdev = sdio;

    sdio_power_off(fdev);
}


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_hard_reset
 *
 *      Hard Resets UniFi is possible.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 *
 * Returns:
 * 1 if the SDIO driver is not capable of doing a hard reset.
 * 0 if a hard reset was successfully performed.
 * -EIO if an I/O error occured while re-initializing the
 * card.  This is a fatal, non-recoverable error.
 * -ENODEV if the card is no longer present.
 * ---------------------------------------------------------------------------
 */
int unifi_sdio_hard_reset(void *sdio)
{
    struct sdio_dev *fdev = sdio;
    int r;

    /* Hard reset can be disabled by a module parameter */
    r = 1;
    if (disable_hw_reset != 1) {
        r = sdio_hard_reset(fdev);
        if (r < 0) {
            return r;
        }
    }

    /* Set the SDIO bus width after a hard reset */
    if (buswidth == 1) {
        unifi_info(NULL, "Setting SDIO bus width to 1\n");
        sdio_set_bus_width(fdev, buswidth);
    } else if (buswidth == 4) {
        unifi_info(NULL, "Setting SDIO bus width to 4\n");
        sdio_set_bus_width(fdev, buswidth);
    }

    return r;

} /* unifi_sdio_hard_reset() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_get_info
 *
 *      Return UniFi information read by the SDIO driver.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 *      param_type      The enum value for the required information
 *      param_value     Pointer to store the returned information
 *
 * Returns:
 *      UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
int
unifi_sdio_get_info(void *sdio, enum unifi_sdio_request param_type, unsigned int *param_value)
{
    struct sdio_dev *fdev = sdio;

    switch (param_type) {
      case UNIFI_SDIO_IO_BLOCK_SIZE:
        *param_value = fdev->blocksize;
        break;
      case UNIFI_SDIO_VENDOR_ID:
        *param_value = fdev->vendor_id;
        break;
      case UNIFI_SDIO_DEVICE_ID:
        *param_value = fdev->device_id;
        break;
      case UNIFI_SDIO_FUNCTION_NUM:
        *param_value = fdev->function;
        break;
      default:
        return -EINVAL;
    }

    return 0;

} /* unifi_sdio_get_info() */


/*
 * ---------------------------------------------------------------------------
 *  uf_sdio_int_handler
 *
 *      Interrupt callback function for SDIO interrupts.
 *      This is called in kernel context (i.e. not interrupt context).
 *      We retrieve the unifi context pointer and call the main UniFi
 *      interrupt handler.
 *
 *  Arguments:
 *      fdev      SDIO context pointer
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
static void
uf_sdio_int_handler(struct sdio_dev *fdev)
{
    unifi_priv_t *priv = fdev->drv_data;

    unifi_sdio_interrupt_handler(priv->card);
} /* uf_sdio_int_handler() */


/*
 * ---------------------------------------------------------------------------
 *  uf_sdio_probe
 *
 *      Card insert callback.
 *
 * Arguments:
 *      fdev            SDIO context pointer
 *
 * Returns:
 *      UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
static int
uf_sdio_probe(struct sdio_dev *fdev)
{
    unifi_info(NULL, "card inserted\n");

    /* Always override default SDIO bus clock */
    unifi_trace(NULL, UDBG1, "Setting SDIO bus clock to %d kHz\n", sdio_clock);
    sdio_set_max_bus_freq(fdev, 1000 * sdio_clock);

    /* Register this device with the main UniFi driver */
    fdev->drv_data = register_unifi_sdio(fdev, fdev->slot_id, fdev->os_device);

    return fdev->drv_data ? 0 : -ENOMEM;
} /* uf_sdio_probe() */


/*
 * ---------------------------------------------------------------------------
 *  uf_sdio_remove
 *
 *      Card removal callback.
 *
 * Arguments:
 *      fdev            SDIO device
 *
 * Returns:
 *      UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
static void
uf_sdio_remove(struct sdio_dev *fdev)
{
    unifi_info(NULL, "card removed\n");

    sdio_disable_interrupt(fdev);

    /* Clean up the main UniFi driver */
    unregister_unifi_sdio(fdev->slot_id);
} /* uf_sdio_remove */


/*
 * ---------------------------------------------------------------------------
 *  uf_sdio_suspend
 *
 *      System suspend callback.
 *
 * Arguments:
 *      fdev            SDIO device
 *
 * Returns:
 *
 * ---------------------------------------------------------------------------
 */
static void
uf_sdio_suspend(struct sdio_dev *fdev)
{
    unifi_priv_t *priv = fdev->drv_data;

    unifi_trace(NULL, UDBG3, "Suspending...\n");
    /* Pass event to UniFi Driver. */
    unifi_suspend(priv);

} /* uf_sdio_suspend() */


/*
 * ---------------------------------------------------------------------------
 *  uf_sdio_resume
 *
 *      System resume callback.
 *
 * Arguments:
 *      fdev            SDIO device
 *
 * Returns:
 * 
 * ---------------------------------------------------------------------------
 */
static void
uf_sdio_resume(struct sdio_dev *fdev)
{
    unifi_priv_t *priv = fdev->drv_data;

    unifi_trace(NULL, UDBG3, "Resuming...\n");
    /* Pass event to UniFi Driver. */
    unifi_resume(priv);
} /* uf_sdio_resume() */


static struct sdio_id_table unifi_ids[] = {
    {
        .vendor_id = SDIO_MANF_ID_CSR,
        .device_id = SDIO_CARD_ID_UNIFI_1,
        .function  = SDIO_WLAN_FUNC_ID_UNIFI_1,
        .interface = SDD_ANY_IFACE,
    },
    {
        .vendor_id = SDIO_MANF_ID_CSR,
        .device_id = SDIO_CARD_ID_UNIFI_2,
        .function  = SDIO_WLAN_FUNC_ID_UNIFI_2,
        .interface = SDD_ANY_IFACE,
    },
    {
        .vendor_id = SDIO_MANF_ID_CSR,
        .device_id = SDIO_CARD_ID_UNIFI_3,
        .function  = SDIO_WLAN_FUNC_ID_UNIFI_3,
        .interface = SDD_ANY_IFACE,
    },
    {
        .vendor_id = SDIO_MANF_ID_CSR,
        .device_id = SDIO_CARD_ID_UNIFI_4,
        .function  = SDIO_WLAN_FUNC_ID_UNIFI_4,
        .interface = SDD_ANY_IFACE,
    },
    { 0 },
};

static struct sdio_func_driver unifi_driver = {
    .name = "unifi",
    .id_table = unifi_ids,

    .probe  = uf_sdio_probe,
    .remove = uf_sdio_remove,
    .card_int_handler = uf_sdio_int_handler,
    .suspend  = uf_sdio_suspend,
    .resume = uf_sdio_resume,
};


/*
 * ---------------------------------------------------------------------------
 *  uf_sdio_load
 *  uf_sdio_unload
 *
 *      These functions are called from the main module load and unload
 *      functions. They perform the appropriate operations for the monolithic
 *      driver.
 * 
 *  Arguments:
 *      None.
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
int __init
uf_sdio_load(void)
{
    int r;

    printk("Unifi: Using CSR embedded SDIO driver\n");

    r = sdio_driver_register(&unifi_driver);
    if (r) {
        unifi_error(NULL, "Failed to register UniFi SDIO driver: %d\n", r);
        return r;
    }

    return 0;
} /* uf_sdio_load() */



void __exit
uf_sdio_unload(void)
{
    sdio_driver_unregister(&unifi_driver);
} /* uf_sdio_unload() */

