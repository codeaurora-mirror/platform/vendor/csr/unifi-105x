/** @file osa.c
 *
 * Operating System Abstraction main source file
 * 
 *   Copyright (C) Cambridge Silicon Radio Ltd 2007-2008. All rights reserved.
 * 
 * @section DESCRIPTION
 *   Provides abstraction API implementation for typical OS-related functions 
 *   such as malloc, free, etc.
 * 
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 ****************************************************************************/

/** @{
 * @ingroup abstractions
 */
 
/* STANDARD INCLUDES ********************************************************/

/* PROJECT INCLUDES *********************************************************/
#include "abstractions/osa.h"
#include "sme_trace/sme_trace.h"

/* Trace may be dangerous - only enable if sme_trace doesn't use osa_ calls */
/* #undef SME_TRACE_ENABLE    */

/* MACROS *******************************************************************/

/* GLOBAL VARIABLE DEFINITIONS **********************************************/

/* PRIVATE TYPES DEFINITIONS ************************************************/
struct OsaMutex {
    struct semaphore mutex;
};

/* PRIVATE CONSTANT DEFINITIONS *********************************************/

/* PRIVATE VARIABLE DEFINITIONS *********************************************/

/* PRIVATE FUNCTION PROTOTYPES **********************************************/

/* PRIVATE FUNCTION DEFINITIONS *********************************************/

/* PUBLIC FUNCTION DEFINITIONS **********************************************/

void osa_initialise()
{
}

void osa_shutdown()
{
}

/* --------------------------------------------------------------------------
 * Initialise the mutex so that it can be used to lock
 * areas of code
 */
OsaMutex* osa_critical_section_create(void)
{
    OsaMutex* osaMutex = (OsaMutex*)osa_malloc(sizeof(OsaMutex));
    init_MUTEX(&osaMutex->mutex);
    return osaMutex;
}

/* --------------------------------------------------------------------------
 * Destroys the mutex so that the associate resources are freed
 */
void osa_critical_section_destroy(OsaMutex* osaMutex)
{
    osa_free(osaMutex);
}


/* --------------------------------------------------------------------------
 * Marks the code following this function as critical. This means no other
 * context that uses the same critical section handle may execute the code
 */
void osa_critical_section_entry(OsaMutex* osaMutex)
{
    down_interruptible(&osaMutex->mutex);
}


/* --------------------------------------------------------------------------
 * Marks the end of the critical section - many execution contexts may
 * execute the code after this call.
 */
void osa_critical_section_exit(OsaMutex* osaMutex)
{
    up(&osaMutex->mutex);
}

/* --------------------------------------------------------------------------
 * Return time of day in milliseconds
 */
uint32 osa_get_time_of_day_milli_seconds()
{
    uint32 timeMilliseconds;
    timeMilliseconds = jiffies_to_msecs(jiffies);

    return timeMilliseconds;
}

/* --------------------------------------------------------------------------
 * Cause an abnormal termination of the program - verbose version
 */
void osa_panic_verbose(const char * fileName, 
                       int lineNum,
                       osa_panic_code reason)
{
    sme_trace_crit((TR_OSA, "System panic in %s line %d - code %d", 
                    fileName, lineNum, reason));
    WARN_ON(1);
}

/* --------------------------------------------------------------------------
 * Cause an abnormal termination of the program - verbose version
 */
void osa_panic_brief(osa_panic_code reason)
{
    sme_trace_crit((TR_OSA, "System panic - code %d", reason));
    WARN_ON(1);
}

#define BYTES_FOR_SIZE_STORAGE 4

void *sme_osa_malloc(unsigned int sz)
{
    uint8 *p_alloc;

    p_alloc = (uint8*)kmalloc(sz, GFP_KERNEL);
    if (p_alloc == NULL) {
        sme_trace_crit((TR_OSA, "\n*** osa_malloc failed ***\n\n"));
        return NULL;
    }

    return (p_alloc);
}


void *sme_osa_calloc(unsigned int nmemb, unsigned int sz)
{
    void *p_alloc;

    p_alloc = kmalloc(nmemb * sz, GFP_KERNEL);
    if (p_alloc == NULL) {
        return NULL;
    }
    memset(p_alloc, 0, nmemb * sz);

    return p_alloc;
}


void sme_osa_free(void *ptr)
{
    if (!ptr) {
        return;
    }

    kfree(ptr);
}


/** @}
 */
