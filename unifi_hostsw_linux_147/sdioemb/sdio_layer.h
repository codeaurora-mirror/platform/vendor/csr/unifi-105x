/*
 * Internal/private definitions.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef _SDIO_LAYER_H
#define _SDIO_LAYER_H

#include <oska/all.h>

#include <sdioemb/sdio.h>
#include <sdioemb/sdio_api.h>
#include <sdioemb/slot_api.h>

#include "sdio_event_log.h"
#include "sdio_config.h"

#define SDD_MAX_FUNCTIONS (SDIO_MAX_FUNCTIONS + 1) /* extra for uif function */

struct sdio_slot_priv {
    int id;

    unsigned card_present:1;
    unsigned card_powered:1;

    int num_functions;
    struct sdio_dev *functions[SDD_MAX_FUNCTIONS];
    int max_bus_width;
    int max_card_freq;
    int current_clock_freq;
    unsigned supports_high_speed:1;

    struct sdio_card_io_ops io_ops;

    os_spinlock_t lock;

    unsigned active;
    unsigned power;
    unsigned int_enabled;

    int busy;
    int last_function;
#if SDD_CARD_IS_REMOVABLE
    os_thread_t card_detect_thread;
#endif
    os_mutex_t card_mutex;

#ifdef SDD_DEBUG_EVENT_LOG
    struct sdio_event_log event_log;
#endif
};

struct sdio_dev_priv {
    struct sdio_slot *slot;
    uint32_t cis_ptr;
    int max_freq;
    struct sdio_cmd *queued_cmd;
};

void slot_error(struct sdio_slot *slot, const char *fmt, ...);
void slot_warning(struct sdio_slot *slot, const char *fmt, ...);
void slot_info(struct sdio_slot *slot, const char *fmt, ...);
void slot_debug(struct sdio_slot *slot, const char *fmt, ...);

void sdio_core_init(void);
struct sdio_dev *sdio_dev_alloc(struct sdio_slot *slot, int func);
void sdio_dev_free(struct sdio_dev *fdev);
int sdio_dev_add(struct sdio_dev *fdev);
void sdio_dev_del(struct sdio_dev *fdev);

int sdio_card_detect_init(struct sdio_slot *slot);
void sdio_card_detect_exit(struct sdio_slot *slot);
int sdio_card_start(struct sdio_slot *slot);
void sdio_card_stop(struct sdio_slot *slot);
void sdio_card_configure(struct sdio_slot *slot);

int slot_start_cmd(struct sdio_slot *slot, struct sdio_cmd *cmd);
void slot_set_max_bus_freq(struct sdio_slot *slot, int freq);
void slot_set_bus_freq(struct sdio_slot *slot, int freq);
int slot_set_bus_width(struct sdio_slot *slot, int bus_width);

int sdio_func_set_block_size(struct sdio_dev *fdev, int blksz);
void sdio_func_set_max_bus_freq(struct sdio_dev *fdev, int max_freq);

int sdio_cis_read_ptr_reg(struct sdio_dev *fdev, uint32_t addr, uint32_t *ptr);

int sdio_raw_cmd(struct sdio_slot *slot, uint8_t cmd_id, uint32_t arg, unsigned flags,
                 union sdio_response *response);
int cspi_raw_word_cmd(struct sdio_slot *slot, uint8_t cmd_id, uint32_t addr, uint16_t *word);

int sdio_io_read8(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t *data);
int sdio_io_write8(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t data);
int sdio_io_read(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t *data, size_t len);
int sdio_io_write(struct sdio_dev *fdev, int func, uint32_t addr, const uint8_t *data, size_t len);

int cspi_io_read8(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t *data);
int cspi_io_write8(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t data);
int cspi_io_read(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t *data, size_t len);
int cspi_io_write(struct sdio_dev *fdev, int func, uint32_t addr, const uint8_t *data, size_t len);

int cspi_is_enabled(struct sdio_slot *slot);
int cspi_card_init(struct sdio_slot *slot);
int cspi_enable(struct sdio_slot *slot);

extern int sdd_card_poll_interval_ms;
extern int sdd_max_bus_freq;
extern int sdd_max_block_size;

extern os_mutex_t sdio_core_mutex;

/*
 * Hooks for managing OS-specific device structures.
 */
int sdio_os_dev_add(struct sdio_dev *fdev);
void sdio_os_dev_del(struct sdio_dev *fdev);

#endif /* #ifndef _SDIO_LAYER_H */
