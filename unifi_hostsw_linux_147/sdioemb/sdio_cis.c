/*
 * SDIO CIS access.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <sdioemb/sdio_cis.h>

#include "sdio_layer.h"

/**
 * Read a 24 bit CIS pointer register.
 */
int sdio_cis_read_ptr_reg(struct sdio_dev *fdev, uint32_t addr, uint32_t *ptr)
{
    uint32_t cis_ptr = 0;
    int b;

    for (b = 0; b < 3; b++) {
        uint8_t p;
        int ret = fdev->io->read8(fdev, 0, addr + b, &p);
        if (ret < 0)
            return ret;
        cis_ptr |= p << (b * 8);
    }
    *ptr = cis_ptr;
    return 0;
}

/**
 * Read a CIS tuple.
 *
 * Copies the specified function's CIS tuple into a buffer.  The tuple
 * ID and link pointer are not copied.
 *
 * @param fdev  function to read CIS from.
 * @param tuple ID of tuple to find.
 * @param buf   destination buffer for tuple data.
 * @param len   length of \a buf.
 *
 * @returns 0 on success; -ve on error:
 *          -ENXIO - no such tuple found,
 *          -EINVAL - buffer is shorter than the tuple,
 *          -EIO,-ETIMEDOUT - I/O error while reading from card.
 *
 * @ingroup fdriver
 */
int sdio_cis_get_tuple(struct sdio_dev *fdev, uint8_t tuple,
                       void *buf, size_t len)
{
    struct sdio_dev_priv *fdevp = fdev->priv;
    uint8_t *bbuf = buf;
    uint32_t cis_ptr;
    uint8_t tpl, lnk;

    /* find tuple */
    cis_ptr = fdevp->cis_ptr;
    for(;;) {
        int ret;

        if (cis_ptr >= 0x17000) {
            /* Valid CIS should have a CISTPL_END so this shouldn't happen. */
            return -ENXIO;
        }

        ret = fdev->io->read8(fdev, 0, cis_ptr++, &tpl);
        if (ret < 0) {
            return ret;
        }
        ret = fdev->io->read8(fdev, 0, cis_ptr++, &lnk);
        if (ret < 0) {
            return ret;
        }

        if (tpl == CISTPL_END) {
            return -ENXIO;
        }
        if (tpl == tuple) {
            break;
        }
        cis_ptr += lnk;
    }

    if (lnk > len) {
        return -EINVAL;
    }

    /* copy tuple data */
    for (; lnk > 0; lnk--) {
        int ret;

        ret = fdev->io->read8(fdev, 0, cis_ptr++, bbuf++);
        if (ret < 0) {
            return ret;
        }
    }

    return 0;
}
