/*
 * Card detection and initialization.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <sdioemb/sdio_cis.h>

#include "sdio_layer.h"
#include "sdio_config.h"

static struct sdio_card_io_ops sdio_io_ops = {
    sdio_io_read8,
    sdio_io_write8,
    sdio_io_read,
    sdio_io_write,
};

static int sdio_card_init_sequence(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    int ret;
    union sdio_response response;
    uint32_t ocr, rca;
    int timeout;

    /* CMD0. Needed to start some cards. */
    ret = sdio_raw_cmd(slot, 0, 0, SDD_CMD_FLAG_RESP_NONE, &response);
    if (ret)
        goto cmd_error;

    /* CMD5. Read OCR. */
    ret = sdio_raw_cmd(slot, 5, 0, SDD_CMD_FLAG_RESP_R4, &response);
    if (ret)
        goto cmd_error;
    ocr = response.r4;
    if (!(ocr & SDIO_OCR_VOLTAGE_3V3)) {
        slot_error(slot, "card does not support 3.3V");
        goto fatal_card_error;
    }
    slotp->num_functions = (ocr & SDIO_OCR_NUM_FUNCS_MASK) >> SDIO_OCR_NUM_FUNCS_OFFSET;

    /* CMD5. Set operating voltage to 3.3V and wait for card to be
     * ready. */
    for (timeout = SDD_CARD_READY_TIMEOUT_MS; timeout > 0; timeout--) {
        ret = sdio_raw_cmd(slot, 5, SDIO_OCR_VOLTAGE_3V3, SDD_CMD_FLAG_RESP_R4, &response);
        if (ret) {
            goto cmd_error;
        }
        ocr = response.r4;
        if (ocr & SDIO_OCR_CARD_READY) {
            break;
        }
        os_sleep_ms(1);
    }
    if (timeout == 0) {
        slot_error(slot, "card not ready");
        goto fatal_card_error;
    }

    if (slot->type == SDD_SLOT_TYPE_SD) {
        /* CMD3. Set/read RCA. */
        ret = sdio_raw_cmd(slot, 3, 0, SDD_CMD_FLAG_RESP_R6, &response);
        if (ret)
            goto cmd_error;
        rca = response.r6 & 0xffff0000;

        /* CMD7. Select card. */
        ret = sdio_raw_cmd(slot, 7, rca, SDD_CMD_FLAG_RESP_R1B, &response);
        if (ret)
            goto cmd_error;
    }

    /* Successful card initialization. */
    return 0;

  cmd_error:
    return -EIO;
  fatal_card_error:
    return -ENODEV;
}

static int sdio_card_init(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    int t;
    int ret;

    for (t = 0; t < SDD_CARD_INIT_RETRY_MAX; t++) {
        ret = sdio_card_init_sequence(slot);
        if (ret == -ENODEV)
            goto fatal_card_error;
        if (ret == 0)
            break;
    }
    if (t == SDD_CARD_INIT_RETRY_MAX) {
        goto fatal_card_error;
    }

    slotp->io_ops = sdio_io_ops;

    return 0;

  fatal_card_error:
    /* Card isn't functional, present, or is not supported. */
    return -ENODEV;
}

int sdio_card_start(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    int r;

    sdio_event_log(slot, SDD_EVENT_POWER_ON);

    slot->card_power(slot, SDIO_POWER_3V3);
    os_sleep_ms(SDIO_POWER_UP_TIME_MS);
    slotp->card_powered = 1;

    sdio_event_log(slot, SDD_EVENT_CLOCK_FREQ, SDIO_CLOCK_FREQ_MIN);
    slot->clock_freq = SDIO_CLOCK_FREQ_MIN;
    slot->bus_width = 1;

    if (slot->type == SDD_SLOT_TYPE_SPI_CSPI && cspi_is_enabled(slot)) {
        r = cspi_card_init(slot);
    } else {
        r = sdio_card_init(slot);
    }
    if (r) {
        slot_error(slot, "card initialization failed");
    }
    return r;
}

void sdio_card_stop(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;

    sdio_event_log(slot, SDD_EVENT_POWER_OFF);

    slot_set_bus_freq(slot, SDD_BUS_FREQ_OFF);
    slot->card_power(slot, SDIO_POWER_OFF);
    slotp->card_powered = 0;
}

static int sdio_card_read_info_f0(struct sdio_dev *fdev)
{
    struct sdio_dev_priv *fdevp = fdev->priv;
    struct sdio_slot *slot = fdevp->slot;
    struct sdio_slot_priv *slotp = slot->priv;
    uint8_t card_caps, high_speed;
    uint16_t manfid[2];
    uint8_t funce[CISTPL_FUNCE_00_SIZE];
    int ret;

    fdev->io->read8(fdev, 0, SDIO_CCCR_CARD_CAPS, &card_caps);
    if (card_caps & SDIO_CCCR_CARD_CAPS_LSC) {
        slotp->max_bus_width = (card_caps & SDIO_CCCR_CARD_CAPS_4BLS) ? 4 : 1;
    } else {
        slotp->max_bus_width = 4;
    }

    fdev->io->read8(fdev, 0, SDIO_CCCR_HIGH_SPEED, &high_speed);
    slotp->supports_high_speed = (high_speed & SDIO_CCCR_HIGH_SPEED) ? 1 : 0;

    /* read common CIS ptr */
    ret = sdio_cis_read_ptr_reg(fdev, SDIO_CCCR_CIS_PTR, &fdevp->cis_ptr);
    if (ret) {
        return ret;
    }
    if (fdevp->cis_ptr < 0x1000 || fdevp->cis_ptr > 0x17000) {
        slot_error(slot, "invalid common CIS ptr (0x%06x)", fdevp->cis_ptr);
        return -EINVAL;
    }

    /* read manfid from CIS */
    ret = sdio_cis_get_tuple(fdev, CISTPL_MANFID, &manfid, CISTPL_MANFID_SIZE);
    if (ret) {
        return ret;
    }
    fdev->vendor_id = os_le16_to_cpu(manfid[0]);
    fdev->device_id = os_le16_to_cpu(manfid[1]);

    /* read maximum bus clock frequency from CIS */
    slotp->max_card_freq = SDIO_CLOCK_FREQ_MIN;
    if (sdio_cis_get_tuple(fdev, CISTPL_FUNCE, &funce, CISTPL_FUNCE_00_SIZE) == 0) {
        /* decode TPLFE_MAX_TRAN_SPEED */
        static const int val[16] = {
            0 /* reserved */, 10, 12, 13, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 70, 80,
        };
        static const int unit_10hz[8] = {
            10000, 100000, 1000000, /* rest are reserved */
        };
        int max_tran_speed = val[(funce[3] & 0x78) >> 3] * unit_10hz[funce[3] & 0x7];
        if (max_tran_speed > 0) {
            if (max_tran_speed > slot->caps.max_bus_freq) {
                max_tran_speed = slot->caps.max_bus_freq;
            }
            if (sdd_max_bus_freq != 0 && max_tran_speed > sdd_max_bus_freq) {
                max_tran_speed = sdd_max_bus_freq;
            }
            slotp->max_card_freq = max_tran_speed;
        } else {
            slot_warning(slot, "invalid TPLFE_MAX_TRAN_SPEED (%02x)", funce[3]);
        }
    }

    slot_debug(slot, "initialized card %04x:%04x", fdev->vendor_id, fdev->device_id);

    return 0;
}

static int sdio_card_read_info_fn(struct sdio_dev *fdev)
{
    struct sdio_dev_priv *fdevp = fdev->priv;
    struct sdio_slot *slot = fdevp->slot;
    int func = fdev->function;
    uint8_t funce_dat[CISTPL_FUNCE_01_SIZE];
    int ret;
    
    /* read func CIS ptr */
    ret = sdio_cis_read_ptr_reg(fdev, SDIO_FBR_CIS_PTR(func), &fdevp->cis_ptr);
    if (ret) {
        return ret;
    }
    if (fdevp->cis_ptr < 0x1000 || fdevp->cis_ptr > 0x17000) {
        slot_error(slot, "invalid function %d CIS ptr (0x%06x)", func, fdevp->cis_ptr);
        return -EINVAL;
    }

    /* iface type */
    ret = fdev->io->read8(fdev, 0, SDIO_FBR_STD_IFACE(func), &fdev->interface);
    if (ret) {
        return ret;
    }

    /* max blocksize from CIS */
    ret = sdio_cis_get_tuple(fdev, CISTPL_FUNCE, funce_dat, CISTPL_FUNCE_01_SIZE);
    if (ret) {
        return ret;
    }
    fdev->max_blocksize = (funce_dat[0x0c] & 0xff) | ((funce_dat[0x0d] & 0xff) << 8);

    slot_debug(slot, "F%d: iface: %02x, blk size: %d",
               func, fdev->interface, fdev->max_blocksize);

    return 0;
}

static int sdio_card_read_info(struct sdio_dev *fdev)
{
    struct sdio_slot *slot = fdev->priv->slot;
    struct sdio_slot_priv *slotp = slot->priv;
    int ret;

    if (fdev->function == 0) {
        return sdio_card_read_info_f0(fdev);
    }

    if (fdev->function != SDD_UIF_FUNC) {
        ret = sdio_card_read_info_fn(fdev);
        if (ret) {
            return ret;
        }
    }

    fdev->vendor_id = slotp->functions[0]->vendor_id;
    fdev->device_id = slotp->functions[0]->device_id;

    return 0;
}

static void sdio_card_configure_f0(struct sdio_dev *fdev)
{
    struct sdio_slot *slot = fdev->priv->slot;
    struct sdio_slot_priv *slotp = slot->priv;
    int bus_width = 1;

    /*
     * Ensure F0 register are in a sensible state:
     *  - disable all functions.
     *  - disable card detect resistor.
     *  - enable continuous SPI interrupt.
     */
    fdev->io->write8(fdev, 0, SDIO_CCCR_IO_EN, 0x00);
    fdev->io->write8(fdev, 0, SDIO_CCCR_BUS_IFACE_CNTL,
                     SDIO_CCCR_BUS_IFACE_CNTL_CD_R_DISABLE
                     | SDIO_CCCR_BUS_IFACE_CNTL_ECSI);

    if (slot->type == SDD_SLOT_TYPE_SD) {
        /* Configure 4 bit mode if supported by host and card. */
        if (slot->caps.max_bus_width == 4 && slotp->max_bus_width == 4) {
            bus_width = 4;
        }
        slot_set_bus_width(slot, bus_width);
    }

    /* Set master and per-function interrupt enables. */
    fdev->io->write8(fdev, 0, SDIO_CCCR_INT_EN, (1 << (slotp->num_functions + 1)) - 1);
}

static void sdio_card_configure_func(struct sdio_dev *fdev)
{
    if (fdev->function == 0) {
        sdio_card_configure_f0(fdev);
    }

    sdio_idle_function(fdev);
}

static int sdio_card_init_func(struct sdio_slot *slot, int func)
{
    struct sdio_dev *fdev;
    struct sdio_dev_priv *fdevp;
    int ret = -EIO;

    fdev = sdio_dev_alloc(slot, func);
    if (!fdev) {
        return -ENOMEM;
    }
    fdevp = fdev->priv;

    ret = sdio_card_read_info(fdev);
    if (ret) {
        goto error;
    }

    return 0;

  error:
    sdio_dev_free(fdev);
    return ret;
}

void sdio_card_configure(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    int f;

    /*
     * Try enabling CSPI mode if the slot supports it.
     */
    if (slot->type == SDD_SLOT_TYPE_SPI_CSPI) {
        cspi_enable(slot);
    }

    for (f = 0; f < SDD_MAX_FUNCTIONS; f++) {
        struct sdio_dev *fdev = slotp->functions[f];
        if (fdev != NULL) {
            sdio_card_configure_func(fdev);
            fdev->blocksize = 0;
            sdio_func_set_block_size(fdev, 0);
        }
    }
}

/**
 * Notify the SDIO layer that a card has been inserted.
 *
 * Callable from: thread context.
 *
 * @param slot the slot for the inserted card.
 *
 * @ingroup sdriver
 */
void sdio_card_inserted(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    int f;

    os_mutex_lock(&sdio_core_mutex);

    if (slotp->card_present)
        goto out;

    slot_info(slot, "card inserted");

    slotp->card_present = 1;

    if (sdio_card_start(slot) < 0) {
        goto out;
    }
    slot_debug(slot, "card has %d functions", slotp->num_functions);

    if (sdio_card_init_func(slot, 0) < 0) {
        goto out;
    }
    for (f = 1; f <= slotp->num_functions; f++) {
        sdio_card_init_func(slot, f);
    }
    sdio_card_init_func(slot, SDD_UIF_FUNC);
    sdio_card_configure(slot);

    /*
     * Add successfully initialized functions to the core.
     */
    for (f = 0; f < SDD_MAX_FUNCTIONS; f++) {
        struct sdio_dev *fdev = slotp->functions[f];
        if (fdev) {
            sdio_dev_add(fdev);
        }
    }

  out:
    os_mutex_unlock(&sdio_core_mutex);
}

/**
 * Notify the SDIO layer that a card has been removed
 *
 * Callable from: thread context.
 *
 * @param slot the slot for the removed card.
 *
 * @ingroup sdriver
 */
void sdio_card_removed(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    int f;

    os_mutex_lock(&sdio_core_mutex);

    if (!slotp->card_present) {
        goto out;
    }

    slot_info(slot, "card removed");

    slotp->card_present = 0;

    for (f = SDD_MAX_FUNCTIONS-1; f >= 0; f--) {
        struct sdio_dev *fdev = slotp->functions[f];
        if (fdev) {
            sdio_dev_del(fdev);
        }
    }
    for (f = 0; f < SDD_MAX_FUNCTIONS; f++) {
        struct sdio_dev *fdev = slotp->functions[f];
        sdio_dev_free(fdev);
    }

    sdio_card_stop(slot);

  out:
    os_mutex_unlock(&sdio_core_mutex);
}

/**
 * Perform a hard reset of the card (if the slot is capable) and
 * reinitialize the card.
 *
 * @param fdev an SDIO device of the card.
 *
 * @return 0 if a hard reset was performed and the card successfully
 * reinitialized.
 * @return 1 if the slot is not capable of hard resets.
 * @return -ve if the reinitialization failed.
 *
 * @ingroup fdriver
 */
int sdio_hard_reset(struct sdio_dev *fdev)
{
    struct sdio_slot *slot = fdev->priv->slot;
    struct sdio_slot_priv *slotp = slot->priv;
    int reset;

    if (!slot->hard_reset) {
        return 1;
    }

    os_mutex_lock(&slotp->card_mutex);

    reset = slot->hard_reset(slot);
    if (reset == 0) {
        slotp->current_clock_freq = 0; /* slot driver may have changed the clock */
        sdio_event_log(slot, SDD_EVENT_RESET);
        reset = sdio_card_start(slot);
        if (reset == 0) {
            sdio_card_configure(slot);
            sdio_func_set_max_bus_freq(fdev, fdev->priv->max_freq);
        }
    }

    os_mutex_unlock(&slotp->card_mutex);

    return reset;
}

#if SDD_CARD_IS_REMOVABLE

static void sdio_card_detect_thread_func(void *data)
{
    struct sdio_slot *slot = data;
    struct sdio_slot_priv *slotp = slot->priv;

    slotp->card_present = 0;

    while (!os_thread_should_stop(&slotp->card_detect_thread)) {
        int card_present = slot->card_present(slot);
        if (card_present) {
            sdio_card_inserted(slot);
        }
        if (!card_present) {
            sdio_card_removed(slot);
        }
        os_try_suspend_thread(&slotp->card_detect_thread);
        os_sleep_ms(sdd_card_poll_interval_ms);
    }
}

/* Called by the driver init, starts the card manager thread. */
int sdio_card_detect_init(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    char name[12];

    snprintf(name, sizeof(name), "sdio_cd/%d", slotp->id);

    return os_thread_create(&slotp->card_detect_thread, name,
                            sdio_card_detect_thread_func, slot);
}

void sdio_card_detect_exit(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;

    os_thread_stop(&slotp->card_detect_thread, NULL);

    sdio_card_removed(slot);
}

#else /* !SDD_CARD_IS_REMOVABLE */

int sdio_card_detect_init(struct sdio_slot *slot)
{
    sdio_card_inserted(slot);
    return 0;
}

void sdio_card_detect_exit(struct sdio_slot *slot)
{
    sdio_card_removed(slot);
}

#endif
