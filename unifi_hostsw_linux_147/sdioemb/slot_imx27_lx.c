/* 
 * Linux i.MX27 SDHC driver.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/dma-mapping.h>
#include <linux/device.h>
#include <linux/kernel-compat.h>

#include <asm/io.h>
#include <asm/dma.h>
#include <asm/delay.h>
#include <asm/arch/clock.h>

#include "slot_imx27.h"
#include "slot_imx27_lx.h"

extern void gpio_sdhc_active(int module);
extern void gpio_sdhc_inactive(int module);

struct imx_sdio_controller {
    struct platform_device *pdev;
    struct resource *       iomem_resource;
    uint32_t                base_phys;
    void __iomem *          base;
    int                     irq;
    enum mxc_clocks         clock_id;
    spinlock_t              lock;
    struct sdio_cmd *       current_cmd;
    int                     cur_bus_width;
    uint32_t                int_ctrl;
    int                     dma;
    enum dma_data_direction dma_dir;
    dma_addr_t              dma_addr;
};

static void imx_sdio_hw_enable_int(struct imx_sdio_controller *sdioc, uint32_t ints)
{
    unsigned long flags;

    spin_lock_irqsave(&sdioc->lock, flags);

    sdioc->int_ctrl |= ints;
    writel(sdioc->int_ctrl, sdioc->base + SDHC_INT_CTRL);

    spin_unlock_irqrestore(&sdioc->lock, flags);
}

static void imx_sdio_hw_disable_int(struct imx_sdio_controller *sdioc, uint32_t ints)
{
    unsigned long flags;

    spin_lock_irqsave(&sdioc->lock, flags);

    sdioc->int_ctrl &= ~ints;
    writel(sdioc->int_ctrl, sdioc->base + SDHC_INT_CTRL);

    spin_unlock_irqrestore(&sdioc->lock, flags);
}

static void imx_sdio_hw_start_clock(struct imx_sdio_controller *sdioc)
{
    writel(STR_STP_CLK_START_CLK, sdioc->base + SDHC_STR_STP_CLK);
}

static void imx_sdio_hw_stop_clock(struct imx_sdio_controller *sdioc)
{
    unsigned timeout = 10000; /* 10 ms */

    writel(STR_STP_CLK_STOP_CLK, sdioc->base + SDHC_STR_STP_CLK);

    /* Wait for clock to stop. */
    do { 
        uint32_t stat = readl(sdioc->base + SDHC_STATUS);
        if (!(stat & STATUS_CARD_BUS_CLK_RUN)) {
            break;
        }
        udelay(1);
    } while (--timeout);

    if (timeout == 0) {
        dev_err(&sdioc->pdev->dev, "unable to stop MMCCLK\n");
    }
}

static int imx_sdio_hw_set_clock_rate(struct imx_sdio_controller *sdioc, int freq)
{
    int clk_in = mxc_get_clocks(sdioc->clock_id);
    uint32_t prescaler, clk_div;
    int f;

    prescaler = 0;
    while (prescaler <= 16) {
        for (clk_div = 1; clk_div <= 0xF; clk_div++) {
            if (prescaler != 0) {
                f = (clk_in / (clk_div + 1)) / (prescaler * 2);
            } else {
                f = clk_in / (clk_div + 1);
            }
            if (f <= freq) {
                break;
            }
        }
        if (clk_div < 0x10) {
            break;
        }
        if (prescaler == 0) {
            prescaler = 1;
        } else {
            prescaler <<= 1;
        }
    }

    imx_sdio_hw_stop_clock(sdioc);
    writel((prescaler << 4) | clk_div, sdioc->base + SDHC_CLK_RATE);
    imx_sdio_hw_start_clock(sdioc);

    return f;
}

static void imx_sdio_hw_start(struct imx_sdio_controller *sdioc)
{
    int i;

    gpio_sdhc_active(sdioc->pdev->id);
    mxc_clks_enable(sdioc->clock_id);

    /* Reset the controller. */
    writel(STR_STP_CLK_MMCSD_RESET, sdioc->base + SDHC_STR_STP_CLK);
    writel(STR_STP_CLK_MMCSD_RESET | STR_STP_CLK_STOP_CLK, sdioc->base + SDHC_STR_STP_CLK);
    for (i = 0; i < 8; i++) {
        writel(STR_STP_CLK_STOP_CLK, sdioc->base + SDHC_STR_STP_CLK);
    }

    writel(64, sdioc->base + SDHC_RES_TO);
    writel(READ_TO_RECOMMENDED, sdioc->base + SDHC_READ_TO);

    imx_sdio_hw_enable_int(sdioc, INT_CTRL_END_CMD_RES);
}

static void imx_sdio_hw_stop(struct imx_sdio_controller *sdioc)
{
    imx_sdio_hw_disable_int(sdioc, INT_CTRL_INT_EN_MASK);
    imx_sdio_hw_stop_clock(sdioc);

    mxc_clks_disable(sdioc->clock_id);
    gpio_sdhc_inactive(sdioc->pdev->id);
}

static uint32_t imx_sdio_hw_read_status_and_clear(struct imx_sdio_controller *sdioc)
{
    uint32_t status;

    status = readl(sdioc->base + SDHC_STATUS);
    writel(status, sdioc->base + SDHC_STATUS);
    return status;
}

static uint32_t imx_sdio_hw_response_r1(struct imx_sdio_controller *sdioc)
{
    uint32_t r0, r1, r2;
    r0 = readl(sdioc->base + SDHC_RES_FIFO);
    r1 = readl(sdioc->base + SDHC_RES_FIFO);
    r2 = readl(sdioc->base + SDHC_RES_FIFO);
    return ((r0 & 0xff) << 24) | (r1 << 8) | (r2 >> 8);
}

static void imx_sdio_cmd_complete(struct sdio_slot *slot, uint32_t status)
{
    struct imx_sdio_controller *sdioc = slot->drv_data;
    struct sdio_cmd *cmd = sdioc->current_cmd;

    /* Check status for error bits. */
    if (status & STATUS_ERR_MASK) {
        if (status & STATUS_TIME_OUT_RESP) {
            cmd->status = SDD_CMD_ERR_CMD_TIMEOUT;
        } else if (status & STATUS_RESP_CRC_ERR) {
            cmd->status = SDD_CMD_ERR_CMD_TIMEOUT;
        } else if (status & STATUS_ERR_DATA_MASK) {
            if (status & STATUS_TIME_OUT_READ) {
                cmd->status = SDD_CMD_ERR_DAT_TIMEOUT;
            } else {
                cmd->status = SDD_CMD_ERR_DAT_CRC;
            }
        }
    } else {
        cmd->status = SDD_CMD_OK;
    }

    if (cmd->data) {
        mxc_dma_disable(sdioc->dma);
        dma_unmap_single(&sdioc->pdev->dev, sdioc->dma_addr,
                         sdioc->current_cmd->len, sdioc->dma_dir);
    }

    /* Read response if it's valid. */
    if (!(cmd->status & SDD_CMD_ERR_CMD)) {
        switch (cmd->flags & SDD_CMD_FLAG_RESP_MASK) {
        case SDD_CMD_FLAG_RESP_NONE:
            break;
        case SDD_CMD_FLAG_RESP_R1:
        case SDD_CMD_FLAG_RESP_R1B:
        case SDD_CMD_FLAG_RESP_R4:
        case SDD_CMD_FLAG_RESP_R5:
        case SDD_CMD_FLAG_RESP_R5B:
        case SDD_CMD_FLAG_RESP_R6:
            cmd->sdio.response.r1 = imx_sdio_hw_response_r1(sdioc);
            break;
        default:
            dev_err(&sdioc->pdev->dev, "response format not supported\n");
        }
    }

    sdio_cmd_complete(slot, cmd);
}

irqreturn_t imx_sdio_int_handler(int irq, void *dev_id)
{
    struct sdio_slot *slot = dev_id;
    struct imx_sdio_controller *sdioc = slot->drv_data;
    struct sdio_cmd *cmd = sdioc->current_cmd;
    uint32_t status;

    status = imx_sdio_hw_read_status_and_clear(sdioc);

    if (status & STATUS_SDIO_INT_ACTIVE) {
        uint32_t int_ctrl = readl(sdioc->base + SDHC_INT_CTRL);
        if (int_ctrl & INT_CTRL_SDIO_IRQ_EN) {
            sdio_interrupt(slot);
        }
    }

    if (status & STATUS_END_CMD_RESP) {
        if (!cmd->data) {
            imx_sdio_cmd_complete(slot, status);
        }
    }

    if (status & STATUS_WRITE_OP_DONE) {
        imx_sdio_hw_disable_int(sdioc, INT_CTRL_WRITE_OP_DONE);
        imx_sdio_cmd_complete(slot, status);
    }

    return IRQ_HANDLED;
}

static void imx_sdio_dma_int_handler(void *devid, int error, unsigned int cnt)
{
    struct sdio_slot *slot = devid;
    struct imx_sdio_controller *sdioc = slot->drv_data;
    uint32_t status;

    status = imx_sdio_hw_read_status_and_clear(sdioc);

    /* For write operations, wait for WRITE_OP_DONE by enabling the
     * interrupt if required. */
    if (!error && sdioc->dma_dir == DMA_TO_DEVICE && !(status & STATUS_WRITE_OP_DONE)) {
        imx_sdio_hw_enable_int(sdioc, INT_CTRL_WRITE_OP_DONE);
    } else {
        imx_sdio_cmd_complete(slot, status);
    }
}

static void imx_sdio_setup_dma(struct sdio_slot *slot)
{
    struct imx_sdio_controller *sdioc = slot->drv_data;
    mxc_dma_device_t dma_id = 0;

    mxc_dma_free(sdioc->dma);
    if (sdioc->pdev->id == 0) {
        if (slot->bus_width == 4) {
            dma_id = MXC_DMA_MMC1_WIDTH_4;
        } else {
            dma_id = MXC_DMA_MMC1_WIDTH_1;
        }
    } else {
        if (slot->bus_width == 4) {
            dma_id = MXC_DMA_MMC2_WIDTH_4;
        } else {
            dma_id = MXC_DMA_MMC2_WIDTH_1;
        }
    }
    sdioc->dma = mxc_dma_request(dma_id, slot->name);
    if (sdioc->dma < 0) {
        dev_err(&sdioc->pdev->dev, "cannot allocate DMA channel\n");
    }
    mxc_dma_callback_set(sdioc->dma, imx_sdio_dma_int_handler, slot);
}

static int imx_sdio_set_bus_freq(struct sdio_slot *slot, int clk)
{
    struct imx_sdio_controller *sdioc = slot->drv_data;
    
    if (clk == SDD_BUS_FREQ_OFF || clk == SDD_BUS_FREQ_IDLE) {
        imx_sdio_hw_stop_clock(sdioc);
        return 0;
    }
    return imx_sdio_hw_set_clock_rate(sdioc, clk);
}

static int imx_sdio_start_cmd(struct sdio_slot *slot, struct sdio_cmd *cmd)
{
    struct imx_sdio_controller *sdioc = slot->drv_data;
    uint32_t nob, blk_len, cmd_dat_ctrl = 0;

    sdioc->current_cmd = cmd;

    if (sdioc->cur_bus_width != slot->bus_width) {
        imx_sdio_setup_dma(slot);
    }

    /* The first command (which will always be a CMD0) must be
     * preceeded by at least 64 clocks. */
    if (cmd->sdio.cmd == 0) {
        cmd_dat_ctrl |= CMD_DAT_CTRL_INIT;
    }

    if (slot->bus_width == 4) {
        cmd_dat_ctrl |= CMD_DAT_CTRL_BUS_WIDTH_4;
    }

    switch (cmd->flags & SDD_CMD_FLAG_RESP_MASK) {
    case SDD_CMD_FLAG_RESP_NONE:
        cmd_dat_ctrl |= CMD_DAT_CTRL_RESP_NONE;
        break;
    case SDD_CMD_FLAG_RESP_R1:
    case SDD_CMD_FLAG_RESP_R1B:
    case SDD_CMD_FLAG_RESP_R5:
    case SDD_CMD_FLAG_RESP_R5B:
    case SDD_CMD_FLAG_RESP_R6:
        cmd_dat_ctrl |= CMD_DAT_CTRL_RESP_R1_R5_R6;
        break;
    case SDD_CMD_FLAG_RESP_R2:
        cmd_dat_ctrl |= CMD_DAT_CTRL_RESP_R2;
        break;
    case SDD_CMD_FLAG_RESP_R3:
    case SDD_CMD_FLAG_RESP_R4:
        cmd_dat_ctrl |= CMD_DAT_CTRL_RESP_R3_R4;
        break;
    }

    if (cmd->data) {
        mxc_dma_requestbuf_t dma_request;

        cmd_dat_ctrl |= CMD_DAT_CTRL_DATA_ENABLE;
        cmd_dat_ctrl |= (cmd->flags & SDD_CMD_FLAG_READ) ? 0 : CMD_DAT_CTRL_WRITE;

        if (cmd->len < cmd->owner->blocksize) {
            blk_len = cmd->len;
            nob = 1;
        } else {
            blk_len = cmd->owner->blocksize;
            nob = cmd->len / blk_len;
        }

        sdioc->dma_dir  = (cmd->flags & SDD_CMD_FLAG_READ) ? DMA_FROM_DEVICE : DMA_TO_DEVICE;
        sdioc->dma_addr = dma_map_single(&sdioc->pdev->dev, cmd->data, cmd->len, sdioc->dma_dir);

        if (sdioc->dma_dir == DMA_FROM_DEVICE) {
            dma_request.src_addr = sdioc->base_phys + SDHC_BUFFER_ACCESS;
            dma_request.dst_addr = sdioc->dma_addr;
            dma_request.num_of_bytes = cmd->len;
            mxc_dma_config(sdioc->dma, &dma_request, 1, MXC_DMA_MODE_READ);
        } else {
            dma_request.src_addr = sdioc->dma_addr;
            dma_request.dst_addr = sdioc->base_phys + SDHC_BUFFER_ACCESS;
            dma_request.num_of_bytes = cmd->len;
            mxc_dma_config(sdioc->dma, &dma_request, 1, MXC_DMA_MODE_WRITE);
        }
        mxc_dma_enable(sdioc->dma);
    } else {
        nob = blk_len = 0;
    }

    /* Start the command. */
    if (blk_len) {
        writel(blk_len, sdioc->base + SDHC_BLK_LEN);
    }
    if (nob) {
        writel(nob, sdioc->base + SDHC_NOB);
    }
    writel(cmd->sdio.cmd, sdioc->base + SDHC_CMD);
    writel(cmd->sdio.arg, sdioc->base + SDHC_ARG);
    writel(cmd_dat_ctrl, sdioc->base + SDHC_CMD_DAT_CTRL);

    return 0;
}

static int imx_sdio_card_present(struct sdio_slot *slot)
{
    struct imx_sdio_controller *sdioc = slot->drv_data;
    struct imx_sdio_plat_data *pdata = sdioc->pdev->dev.platform_data;
    
    return pdata->card_present(sdioc->pdev);
}

static int imx_sdio_card_power(struct sdio_slot *slot, enum sdio_power power)
{
    struct imx_sdio_controller *sdioc = slot->drv_data;
    struct imx_sdio_plat_data *pdata = sdioc->pdev->dev.platform_data;
    
    return pdata->card_power(sdioc->pdev, power);
}

static void imx_sdio_enable_card_int(struct sdio_slot *slot)
{
    struct imx_sdio_controller *sdioc = slot->drv_data;

    imx_sdio_hw_enable_int(sdioc, INT_CTRL_SDIO_IRQ_EN);
}

static void imx_sdio_disable_card_int(struct sdio_slot *slot)
{
    struct imx_sdio_controller *sdioc = slot->drv_data;

    imx_sdio_hw_disable_int(sdioc, INT_CTRL_SDIO_IRQ_EN);
}

static int imx_sdio_probe(struct device *dev)
{
    struct platform_device *pdev = to_platform_device(dev);
    struct imx_sdio_plat_data *pdata = dev->platform_data;
    struct sdio_slot *slot;
    struct imx_sdio_controller *sdioc;
    struct resource *res;
    int len, ret;

    slot = sdio_slot_alloc(sizeof(struct imx_sdio_controller));
    if (!slot) {
        return -ENOMEM;
    }
    dev_set_drvdata(dev, slot);

    strcpy(slot->name, dev->bus_id);
    slot->set_bus_freq     = imx_sdio_set_bus_freq;
    slot->start_cmd        = imx_sdio_start_cmd;
    slot->card_present     = imx_sdio_card_present;
    slot->card_power       = imx_sdio_card_power;
    slot->enable_card_int  = imx_sdio_enable_card_int;
    slot->disable_card_int = imx_sdio_disable_card_int;

    slot->caps.max_bus_width = pdata->max_bus_width;

    sdioc = slot->drv_data;

    sdioc->pdev = pdev;

    res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
    len = res->end - res->start + 1;
    sdioc->iomem_resource = request_mem_region(res->start, len, "slot_imx27");
    if (!sdioc->iomem_resource) {
        dev_err(dev, "memory range %08lx-%08lx in use\n", res->start, res->end);
        ret = -EBUSY;
        goto err_mem_res;
    }
    sdioc->base_phys = res->start;
    sdioc->base = ioremap(sdioc->base_phys, len);
    if (!sdioc->base) {
        ret = -ENOMEM;
        goto err_ioremap;
    }

    sdioc->irq = platform_get_irq(pdev, 0);
    ret = request_irq(sdioc->irq, imx_sdio_int_handler, 0, slot->name, slot);
    if (ret) {
        dev_err(dev, "irq %d in use\n", sdioc->irq);
        goto err_irq;
    }

    if (pdev->id == 0) {
        sdioc->clock_id = SDHC1_CLK;
    } else {
        sdioc->clock_id = SDHC2_CLK;
    }

    spin_lock_init(&sdioc->lock);

    imx_sdio_hw_start(sdioc);

    ret = sdio_slot_register(slot);
    if (ret) {
        goto err_register;
    }

    return 0;

  err_register:
    imx_sdio_hw_stop(sdioc);
    free_irq(sdioc->irq, slot);
  err_irq:
    iounmap(sdioc->base);
  err_ioremap:
    release_resource(sdioc->iomem_resource);
  err_mem_res:
    sdio_slot_free(slot);
    return ret;
}

static int imx_sdio_remove(struct device *dev)
{
    struct sdio_slot *slot = dev_get_drvdata(dev);
    struct imx_sdio_controller *sdioc = slot->drv_data;

    sdio_slot_unregister(slot);

    imx_sdio_hw_stop(sdioc);

    mxc_dma_free(sdioc->dma);
    free_irq(sdioc->irq, slot);
    iounmap(sdioc->base);

    release_resource(sdioc->iomem_resource);

    sdio_slot_free(slot);

    return 0;
}

static struct device_driver imx27_sdio_driver = {
    .name   = "imx27-sdio",
    .bus    = &platform_bus_type,
    .probe  = imx_sdio_probe,
    .remove = imx_sdio_remove,
};

static int __init slot_imx27_init(void)
{
    return driver_register(&imx27_sdio_driver);
}

static void __exit slot_imx27_exit(void)
{
    driver_unregister(&imx27_sdio_driver);
}

module_init(slot_imx27_init);
module_exit(slot_imx27_exit);

MODULE_DESCRIPTION("i.MX27 SDHC slot driver");
MODULE_AUTHOR("Cambridge Silicon Radio Ltd.");
MODULE_LICENSE("GPL and additional rights");
