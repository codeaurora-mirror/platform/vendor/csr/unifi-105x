/*
 * Freescale i.MX27 ADS SDHC driver.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <asm/arch/pmic_power.h>

#include "slot_imx27_lx.h"

static int imx27ads_sdio_card_present(struct platform_device *pdev)
{
    uint16_t bstat;

    bstat = readw(PBC_BSTAT1_REG);
    if (pdev->id == 0) {
        return !(bstat & PBC_BSTAT_SD1_DET);
    } else {
        return !(bstat & PBC_BSTAT_SD2_DET);
    }
}

static int imx27ads_sdio_card_power(struct platform_device *pdev, enum sdio_power power)
{
    t_regulator_voltage voltage;

    switch (power) {
    case SDIO_POWER_OFF:
        if (pdev->id == 0) {
            pmic_power_regulator_set_lp_mode(REGU_VMMC1,
                                             LOW_POWER_EN);
            pmic_power_regulator_off(REGU_VMMC1);
        } else {
            pmic_power_regulator_set_lp_mode(REGU_VMMC2,
                                             LOW_POWER_EN);
            pmic_power_regulator_off(REGU_VMMC2);
        }
        dev_dbg(&pdev->dev, "power off\n");
        break;
    case SDIO_POWER_3V3:
        if (pdev->id == 0) {
            voltage.vmmc1 = VMMC1_3V;
            pmic_power_regulator_set_voltage(REGU_VMMC1, voltage);
            pmic_power_regulator_set_lp_mode(REGU_VMMC1,
                                             LOW_POWER_DISABLED);
            pmic_power_regulator_on(REGU_VMMC1);
        } else {
            voltage.vmmc2 = VMMC2_3V;
            pmic_power_regulator_set_voltage(REGU_VMMC2, voltage);
            pmic_power_regulator_set_lp_mode(REGU_VMMC2,
                                             LOW_POWER_DISABLED);
            pmic_power_regulator_on(REGU_VMMC2);
        }
        dev_dbg(&pdev->dev, "power on\n");
        break;
    default:
        return -EINVAL;
    }

    return 0;
}

static struct resource sdhc1_resources[] = {
    {
        .start = SDHC1_BASE_ADDR,
        .end   = SDHC1_BASE_ADDR + SZ_4K -1,
        .flags = IORESOURCE_MEM,
    },
    {
        .start = INT_SDHC1,
        .end   = INT_SDHC1,
        .flags = IORESOURCE_IRQ,
    },
};

static struct imx_sdio_plat_data sdhc1_plat_data = {
    .max_bus_width = 4,
    .card_present  = imx27ads_sdio_card_present,
    .card_power    = imx27ads_sdio_card_power,
};

static struct resource sdhc2_resources[] = {
    {
        .start = SDHC2_BASE_ADDR,
        .end   = SDHC2_BASE_ADDR + SZ_4K -1,
        .flags = IORESOURCE_MEM,
    },
    {
        .start = INT_SDHC2,
        .end   = INT_SDHC2,
        .flags = IORESOURCE_IRQ,
    },
};

static struct imx_sdio_plat_data sdhc2_plat_data = {
    .max_bus_width = 4,
    .card_present  = imx27ads_sdio_card_present,
    .card_power    = imx27ads_sdio_card_power,
};

static struct platform_device imx27ads_sdio_devices[] = {
    {
        .name          = "imx27-sdio",
        .id            = 0,
        .num_resources = ARRAY_SIZE(sdhc1_resources),
        .resource      = sdhc1_resources,
        .dev           = {
            .platform_data = &sdhc1_plat_data,
        },
    },
    {
        .name          = "imx27-sdio",
        .id            = 1,
        .num_resources = ARRAY_SIZE(sdhc2_resources),
        .resource      = sdhc2_resources,
        .dev           = {
            .platform_data = &sdhc2_plat_data,
        },
    },
};

static int __init slot_imx27ads_init(void)
{
    int i;

    for (i = 0; i < ARRAY_SIZE(imx27ads_sdio_devices); i++) {
        platform_device_register(&imx27ads_sdio_devices[i]);
    }
    return 0;
}

module_init(slot_imx27ads_init);

MODULE_DESCRIPTION("Freescale i.MX27 ADS SDIO slot driver");
MODULE_AUTHOR("Cambridge Silicon Radio Ltd.");
MODULE_LICENSE("GPL and additional rights");
