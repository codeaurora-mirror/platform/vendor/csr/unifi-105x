/*
 * SDIO function control.
 *
 * Copyright (C) 2007-2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include "sdio_layer.h"

/* FIXME: use timeout value in CIS (on 1.1 cards)? */
#define IO_EN_TIMEOUT_MS 500

/**
 * Enable the function.
 *
 * The function's I/O Enable bit is set and I/O Ready is polled until
 * it is set.
 *
 * Callable from: thread context.
 *
 * @param fdev  the SDIO device to enable.
 *
 * @return 0 on success; -ve on error:
 *         -ETIMEDOUT - time out waiting for I/O Ready to be set,
 *         -EIO or -ETIMEDOUT - a register access failed.
 *
 * @ingroup fdriver
 */
int sdio_enable_function(struct sdio_dev *fdev)
{
    struct sdio_slot_priv *slotp = fdev->priv->slot->priv;
    uint8_t io_en, io_rdy;
    int timeout = IO_EN_TIMEOUT_MS;
    int ret;

    os_mutex_lock(&slotp->card_mutex);

    ret = fdev->io->read8(fdev, 0, SDIO_CCCR_IO_EN, &io_en);
    if (ret) {
        goto out_unlock;
    }
    io_en |= 1 << fdev->function;
    ret = fdev->io->write8(fdev, 0, SDIO_CCCR_IO_EN, io_en);
    if (ret) {
        goto out_unlock;
    }

    /* No need to hold the lock while waiting for I/O Ready. */
    os_mutex_unlock(&slotp->card_mutex);

    while (timeout) {
        ret = fdev->io->read8(fdev, 0, SDIO_CCCR_IO_READY, &io_rdy);
        if (ret) {
            return ret;
        }
        if (io_rdy & (1 << fdev->function)) {
            return 0;
        }

        os_sleep_ms(1);
        timeout--;
    }
    return -ETIMEDOUT;

  out_unlock:
    os_mutex_unlock(&slotp->card_mutex);
    return ret;
}

/**
 * Disable the function.
 *
 * The function's I/O Enable bit is cleared.
 *
 * Callable from: thread context.
 *
 * @param fdev  the SDIO device to disable.
 *
 * @return 0 on success; -ve on error:
 *         -EIO or -ETIMEDOUT - a register access failed.
 *
 * @bug To permit a disable/enable sequence to be used as a
 * per-function reset, disabling a function should wait for I/O Ready
 * to be cleared.
 *
 * @ingroup fdriver
 */
int sdio_disable_function(struct sdio_dev *fdev)
{
    struct sdio_slot_priv *slotp = fdev->priv->slot->priv;
    uint8_t io_en;
    int ret;

    os_mutex_lock(&slotp->card_mutex);

    ret = fdev->io->read8(fdev, 0, SDIO_CCCR_IO_EN, &io_en);
    if (ret) {
        goto out;
    }
    io_en &= ~(1 << fdev->function);
    ret = fdev->io->write8(fdev, 0, SDIO_CCCR_IO_EN, io_en);

  out:
    os_mutex_unlock(&slotp->card_mutex);
    return ret;
}

/**
 * Set a function's block size without taking the card mutex.
 *
 * @see sdio_set_block_size()
 */
int sdio_func_set_block_size(struct sdio_dev *fdev, int blksz)
{
    int ret;

    if (blksz == 0) {
        blksz = fdev->max_blocksize < 512 ? fdev->max_blocksize : 512;
        if (sdd_max_block_size && blksz > sdd_max_block_size) {
            blksz = sdd_max_block_size;
        }
    }

    if (fdev->function != SDD_UIF_FUNC && blksz != fdev->blocksize) {
        /* Ignore -EINVAL (OUT_OF_RANGE in R5) on the first byte as
           the block size may be invalid until both bytes are written. */
        ret = fdev->io->write8(fdev, 0, SDIO_FBR_BLK_SIZE(fdev->function),blksz & 0xff);
        if (ret && ret != -EINVAL) {
            return ret;
        }
        ret = fdev->io->write8(fdev, 0, SDIO_FBR_BLK_SIZE(fdev->function)+1, (blksz >> 8) & 0xff);
        if (ret) {
            return ret;
        }
    }
    fdev->blocksize = blksz;

    return 0;
}

/**
 * Set a function's block size.
 *
 * The default block size is the largest supported by both the
 * function and the host, with a maximum of 512 to ensure that
 * arbitrarily sized data transfers use the optimal (least) number of
 * commands.
 *
 * A driver may call this to override the default block size set by
 * the core. This can be used to set a block size greater than the
 * maximum that reported by the card; it is the driver's
 * responsibility to ensure it uses a value that the card supports.
 *
 * Callable from: thread context.
 *
 * @param fdev  the SDIO device.
 * @param blksz new block size or 0 to use the default.
 *
 * @return 0 on success; -ve on error:
 *         -EIO or -ETIMEDOUT - a register access failed.
 *
 * @ingroup fdriver
 */
int sdio_set_block_size(struct sdio_dev *fdev, int blksz)
{
    struct sdio_slot_priv *slotp = fdev->priv->slot->priv;
    int ret;

    os_mutex_lock(&slotp->card_mutex);
    ret = sdio_func_set_block_size(fdev, blksz);
    os_mutex_unlock(&slotp->card_mutex);

    return ret;
}

/**
 * Set the function as idle.
 *
 * When all functions are idle the SD bus will be idled.
 *
 * Functions are set as active (non-idle) when a command is started.
 *
 * @param fdev the SDIO device to set as idle.
 *
 * @see slot_driver::idle_bus.
 *
 * @ingroup fdriver
 */
void sdio_idle_function(struct sdio_dev *fdev)
{
    struct sdio_dev_priv *fdevp = fdev->priv;
    struct sdio_slot *slot = fdevp->slot;
    struct sdio_slot_priv *slotp = slot->priv;
    os_int_status_t istate;

    os_spinlock_lock_intsave(&slotp->lock, &istate);

    slotp->active &= ~(1 << fdev->function);
    if (!slotp->active) {
        slot_set_bus_freq(slot, SDD_BUS_FREQ_IDLE);
    }

    os_spinlock_unlock_intrestore(&slotp->lock, &istate);
}

/**
 * Limit the bus frequency supported by a function.
 *
 * slotp->card_mutex must be held.
 *
 * @see sdio_set_max_bus_freq
 */
void sdio_func_set_max_bus_freq(struct sdio_dev *fdev, int max_freq)
{
    struct sdio_dev_priv *fdevp = fdev->priv;
    struct sdio_slot *slot = fdevp->slot;
    struct sdio_slot_priv *slotp = slot->priv;
    int freq;
    int f;

    if (max_freq == SDD_BUS_FREQ_DEFAULT) {
        max_freq = slotp->max_card_freq;
    }
    fdevp->max_freq = max_freq;

    freq = slotp->max_card_freq;
    for (f = 0; f < SDD_MAX_FUNCTIONS; f++) {
        if (slotp->functions[f]) {
            fdevp = slotp->functions[f]->priv;
            if (fdevp->max_freq != 0 && fdevp->max_freq < freq) {
                freq = fdevp->max_freq;
            }
        }
    }
    slot_set_max_bus_freq(slot, freq);
}

/**
 * Limit the bus frequency supported by a function.
 *
 * The bus frequency used will be the minimum of:
 *   - the speed reported by the card,
 *   - the maximum the slot can support,
 *   - the value set by this function for any of the card's functions.
 *
 * If the frequency to be used is high speed (> 25 MHz), high speed
 * mode will be enabled on the card.
 *
 * Callable from: thread context.
 *
 * @param fdev     the SDIO device.
 * @param max_freq maximum frequency in Hz, or 0 for the maximum
 *                 reported by the card.
 *
 * @ingroup fdriver
 */
void sdio_set_max_bus_freq(struct sdio_dev *fdev, int max_freq)
{
    struct sdio_dev_priv *fdevp = fdev->priv;
    struct sdio_slot *slot = fdevp->slot;
    struct sdio_slot_priv *slotp = slot->priv;

    os_mutex_lock(&slotp->card_mutex);
    sdio_func_set_max_bus_freq(fdev, max_freq);
    os_mutex_unlock(&slotp->card_mutex);
}

/**
 * Set the SDIO bus to 1 bit or 4 bit mode.
 *
 * Callable from: thread context.
 *
 * @param fdev      the SDIO device.
 * @param bus_width the bus width (1 or 4).
 *
 * @return 0 on success (or if the slot is in SPI mode).
 * @return -EINVAL if the slot doesn't support the requested mode.
 * @return -EIO, -ETIMEDOUT if an I/O error occured when writing the
 *         mode to the card.
 *
 * @ingroup fdriver
 */
int sdio_set_bus_width(struct sdio_dev *fdev, int bus_width)
{
    struct sdio_dev_priv *fdevp = fdev->priv;
    struct sdio_slot *slot = fdevp->slot;
    struct sdio_slot_priv *slotp = slot->priv;
    int ret;

    if (bus_width != 1 && bus_width != 4) {
        return -EINVAL;
    }
    if (bus_width > slot->caps.max_bus_width) {
        return -EINVAL;
    }
    if (slot->type != SDD_SLOT_TYPE_SD) {
        return 0;
    }

    os_mutex_lock(&slotp->card_mutex);

    ret = slot_set_bus_width(slot, bus_width);

    os_mutex_unlock(&slotp->card_mutex);

    return ret;
}
