/*
 * Linux SDIO-SPI/CSPI slot driver.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <linux/types.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/netdevice.h>
#include <linux/interrupt.h>
#include <linux/spi/spi.h>
#include <linux/dma-mapping.h>
#include <linux/delay.h>
#include <linux/kernel-compat.h>
#include <linux/jiffies.h>

#include <sdioemb/cspi.h>
#include <sdioemb/sdio_api.h>
#include <sdioemb/slot_api.h>

#include "slot_spi_lx.h"

/* Length (in units of 8 clocks) of the initialization sequence. */
#define SDIO_SPI_INIT_SEQ_LEN 10 /* = 80 clocks */

/* SDIO-SPI bus timings in units of 8 clocks */
#define SDIO_SPI_N_CR_MAX 8 /* max. time between command and response */
#define SDIO_SPI_N_EC_MIN 0 /* min. time after response and CS inactive */

/* Command and response lengths in bytes */
#define SDIO_SPI_CMD_LEN 6
#define SDIO_SPI_R1_LEN  1
#define SDIO_SPI_R4_LEN  5
#define SDIO_SPI_R5_LEN  2

/* SDIO-SPI response bits */
#define SDIO_SPI_R1_IDLE 0x01

/* SDIO-SPI command bits */
#define CMD53_W_FLAG      0x80000000
#define CMD53_BLOCK_MODE  0x08000000

#define CMD53_CRC_LEN    2
#define CMD53_D_RESP_LEN 8

/* CMD53 SDIO_SPI state machine states */
enum spi_cmd53_sm_states {
    CMD53_STATE_AFTER_CMD_WRITE,
    CMD53_STATE_POLL_RESPONSE,
    CMD53_STATE_AFTER_RESPONSE,
    CMD53_STATE_POLL_DATA_READ,
    CMD53_STATE_WRITE_DATA_BLOCK,
    CMD53_STATE_READ_DATA_BLOCK,
    CMD53_STATE_DATA_WRITE_COMPLETED,    
};

/* CMD53 SDIO_SPI state machine data */
struct spi_cmd53_sm_data {
    int status;
    int prev;
    uint8_t *data_p;
    int blocks_remaining;
    int block_size;
    int n_cr;
    unsigned long timeout;
};

struct sdio_spi_controller {
    struct spi_device *spi;
    struct sdio_cmd *current_cmd;
    struct spi_message spi_msg;
    struct spi_transfer spi_trans;
    void *transfer_buf;
    struct spi_cmd53_sm_data cmd53_sm; 
};

/*
 * For testing purposes we can enable big-endian CSPI burst transfers
 * with the cspi_big_endian module parameter.  It's not possible to
 * use big-endian CSPI register accesses without knowing if the
 * register is 16 bit or 8 bit, therefore there's no need to test
 * them.
 */
static int cspi_big_endian = 0;
static void sdio_spi_cmd53_state_machine(void *context);

module_param(cspi_big_endian, int, 0444);
MODULE_PARM_DESC(cspi_big_endian, "1 = use big-endian mode for CPSI");

static void cspi_memcpy_data(void *dest, const void *src, size_t n, u8 cspi_cmd)
{
    if (cspi_big_endian) {
        int i;
        u8 *d = dest;
        const u8 *s = src;

        for (i = 0; i <= (n-2); i += 2) {
            d[i]   = s[i+1];
            d[i+1] = s[i];
        }
        /* odd length, handle the last octet */
        if (i != n) {
            if (cspi_cmd & CSPI_WRITE) {
                d[i]   = 0x00;
                d[i+1] = s[i];
            } else {
                d[i] = s[i+1];
            }
        }
    } else {
        memcpy(dest, src, n);
    }
}

static void cspi_cmd_complete(void *context)
{
    struct sdio_slot *slot = context;
    struct sdio_spi_controller *sdioc = slot->drv_data;
    struct sdio_cmd *cmd = sdioc->current_cmd;
    struct spi_transfer *t;
    uint8_t *rx;
    uint8_t resp = 0;

    if (sdioc->spi_msg.status) {
        cmd->status = SDD_CMD_ERR_CMD_OTHER;
    } else {

        t = list_entry(sdioc->spi_msg.transfers.next, struct spi_transfer, transfer_list);
        rx = t->rx_buf;

        switch (cmd->cspi.cmd & CSPI_TYPE_MASK) {
        case CSPI_READ:
            resp = rx[4 + slot->cspi_reg_pad];
            cmd->cspi.val = rx[4 + slot->cspi_reg_pad + 1]
                | (rx[4 + slot->cspi_reg_pad + 2] << 8);
            break;
        case CSPI_WRITE:
            resp = rx[4];
            break;
        case CSPI_READ | CSPI_BURST:
            resp = rx[6 + slot->cspi_burst_pad];
            cspi_memcpy_data(cmd->data, rx + 6+slot->cspi_burst_pad+1, cmd->len, CSPI_READ);
            break;
        case CSPI_WRITE | CSPI_BURST:
            resp = rx[6];
            break;
        }
        cmd->cspi.response = resp;

        if (resp) {
            cmd->status = SDD_CMD_ERR_DAT_OTHER;
        } else {
            cmd->status = SDD_CMD_OK;
        }
    }

    sdio_cmd_complete(slot, cmd);
}

static int cspi_start_cmd(struct sdio_slot *slot, struct sdio_cmd *cmd)
{
    struct sdio_spi_controller *sdioc = slot->drv_data;
    struct spi_message *m = &sdioc->spi_msg;
    struct spi_transfer *t;
    size_t transfer_len;
    uint8_t *tx;
    int ret = -ENOMEM;

    if (cmd->cspi.cmd & CSPI_BURST) {
        /* Big-endian CSPI data transfers must be multiple of two. */
        size_t data_len = cmd->len;
        if (cspi_big_endian)
            data_len += (cmd->len & 1);
        transfer_len = 1 + 3 + 2 + data_len; /* cmd + addr + len + data */
        if (cmd->cspi.cmd & CSPI_READ) {
            transfer_len += 1 + slot->cspi_burst_pad; /* status byte + padding */
        }
    } else {
        transfer_len = 1 + 3 + 2; /* cmd + addr + data */
        if (cmd->cspi.cmd & CSPI_READ) {
            transfer_len += 1 + slot->cspi_reg_pad; /* status byte + padding */
        }
    }

    t = list_entry(m->transfers.next, struct spi_transfer, transfer_list);
    tx = (uint8_t *)t->tx_buf;

    tx[0] =  cmd->cspi.cmd;
    tx[1] = (cmd->cspi.addr >> 16) & 0xff;
    tx[2] = (cmd->cspi.addr >>  8) & 0xff;
    tx[3] = (cmd->cspi.addr >>  0) & 0xff;
    if (cmd->cspi.cmd & CSPI_BURST) { /* FIXME: set bit to omit len? */
        tx[4] = (cmd->len >> 8) & 0xff;
        tx[5] = (cmd->len >> 0) & 0xff;
        if (cmd->cspi.cmd & CSPI_WRITE) {
            cspi_memcpy_data(tx + 6, cmd->data, cmd->len, CSPI_WRITE);
        }
    } else if (cmd->cspi.cmd & CSPI_WRITE) {
        tx[4] = (cmd->cspi.val >> 0) & 0xff;
        tx[5] = (cmd->cspi.val >> 8) & 0xff;
    }

    t->len      = transfer_len;
    m->complete = cspi_cmd_complete;
    m->context  = slot;

    ret = spi_async(sdioc->spi, m);
    if (ret < 0) {
        dev_dbg(&sdioc->spi->dev, "%s: failed: ret = %d\n", __FUNCTION__, ret);
    }

    return ret;
}


static void sdio_spi_cmd_complete(void *context)
{
    struct sdio_slot *slot = context;
    struct sdio_spi_controller *sdioc = slot->drv_data;
    struct sdio_cmd *cmd = sdioc->current_cmd;
    struct spi_transfer *t;
    uint8_t *rx;
    uint8_t r1;
    int i;

    t = list_entry(sdioc->spi_msg.transfers.next, struct spi_transfer, transfer_list);
    rx = t->rx_buf;

    /* Skip over the init sequence for CMD0s. */
    if (cmd->sdio.cmd == 0) {
        rx += SDIO_SPI_INIT_SEQ_LEN;
    }

    /* Find response. */
    for (i = 0; i <= SDIO_SPI_N_CR_MAX; i++) {
        r1 = rx[SDIO_SPI_CMD_LEN+i];
        if (r1 != 0xff) {
            break;
        }
    }
    if (i > SDIO_SPI_N_CR_MAX) {
        dev_dbg(&sdioc->spi->dev, "no response\n");
        cmd->status = SDD_CMD_ERR_CMD_TIMEOUT;
    } else {
        cmd->status = SDD_CMD_OK;

        if (r1 & ~SDIO_SPI_R1_IDLE) {
            dev_dbg(&sdioc->spi->dev, "error in response (R1 = 0x%02x)\n", r1);
            cmd->status = SDD_CMD_ERR_CMD_OTHER;
        }
        switch (cmd->sdio.cmd) {
        case 5:
            cmd->sdio.response.r4 =
                (rx[SDIO_SPI_CMD_LEN+i+1] << 24) |
                (rx[SDIO_SPI_CMD_LEN+i+2] << 16) |
                (rx[SDIO_SPI_CMD_LEN+i+3] <<  8) |
                (rx[SDIO_SPI_CMD_LEN+i+4] <<  0);
            break;
        case 52:
            cmd->sdio.response.r5 = rx[SDIO_SPI_CMD_LEN+i+1];
            break;
        }
    }

    sdio_cmd_complete(slot, cmd);
}

static int spi_nbytes_transfer(struct sdio_slot *slot, int n_bytes)
{
    struct sdio_spi_controller *sdioc = slot->drv_data;
    struct spi_message *m = &sdioc->spi_msg;
    struct spi_transfer *t;
    uint8_t *tx;
    int ret;

    t = list_entry(m->transfers.next, struct spi_transfer, transfer_list);
    tx = (uint8_t *)t->tx_buf;

    memset(tx, 0xff, n_bytes);

    t->len         = n_bytes;
    m->complete    = sdio_spi_cmd53_state_machine;
    m->context     = slot;
 
    ret = spi_async(sdioc->spi, m);
    if (ret < 0) {
        dev_dbg(&sdioc->spi->dev, "%s: failed: ret = %d\n", __FUNCTION__, ret);
    }
    return ret;
}

static int cmd53_check_data_response_and_busy(uint8_t *rx, int t_len)
{
    int i, k;
    for (i = 0; i < CMD53_D_RESP_LEN; i++) {

        if ((rx[t_len - CMD53_D_RESP_LEN + i] & 0x1f) == 0x5) {
        /* Data Block Response is OK. */
            
            for (k = i + 1; k < CMD53_D_RESP_LEN +1 ; k++) {
                
                if ((rx[t_len - CMD53_D_RESP_LEN + k] & 0xff) != 0) {
                    /* Busy is finished */
                    return 0;
                }
            }
        }
    }
    /* Error condition. */
    return 1;
}

static void sdio_spi_cmd53_state_machine(void *context)
{
    struct sdio_slot *slot = context;
    struct sdio_spi_controller *sdioc = slot->drv_data;
    struct sdio_cmd *cmd = sdioc->current_cmd;
    struct spi_message *m = &sdioc->spi_msg;
    struct spi_transfer *t;
    uint8_t *rx;
    uint8_t *tx;
    uint8_t val;
    int ret;

    t = list_entry(sdioc->spi_msg.transfers.next, struct spi_transfer, transfer_list);
    rx = t->rx_buf;
    tx = (uint8_t *)t->tx_buf;
     
    switch (sdioc->cmd53_sm.status) {

    case CMD53_STATE_AFTER_CMD_WRITE:
        sdioc->cmd53_sm.n_cr = 0;
        sdioc->cmd53_sm.status = CMD53_STATE_POLL_RESPONSE;
        spi_nbytes_transfer(slot, 1);
        break;

    case CMD53_STATE_POLL_RESPONSE:
        sdioc->cmd53_sm.n_cr++;
        val = rx[0];
        if (val != 0xff) {
            /* Check the response. */
            if (sdioc->cmd53_sm.n_cr > SDIO_SPI_N_CR_MAX) {
                dev_dbg(&sdioc->spi->dev, "no response\n");
                cmd->status = SDD_CMD_ERR_CMD_TIMEOUT;
                sdio_cmd_complete(slot, cmd);
            } else {
                cmd->status = SDD_CMD_OK;
                
                if (val & ~SDIO_SPI_R1_IDLE) {
                    dev_dbg(&sdioc->spi->dev, "error in response (VAL = 0x%02x)\n", val);
                    cmd->status = SDD_CMD_ERR_CMD_OTHER;
                } else {
                    sdioc->cmd53_sm.prev = sdioc->cmd53_sm.status;
                    /* Response is ok, we can go further. */
                    if (cmd->sdio.arg & CMD53_W_FLAG) {
                        sdioc->cmd53_sm.status = CMD53_STATE_WRITE_DATA_BLOCK;
                    } else {
                        sdioc->cmd53_sm.status = CMD53_STATE_AFTER_RESPONSE;
                    }
                    if (cmd->sdio.arg & CMD53_BLOCK_MODE) {
                        sdioc->cmd53_sm.blocks_remaining = cmd->sdio.arg & 0x1ff;
                        sdioc->cmd53_sm.block_size = (cmd->len) / (cmd->sdio.arg & 0x1ff);
                    } else {
                        sdioc->cmd53_sm.blocks_remaining = 0;
                        sdioc->cmd53_sm.block_size = cmd->sdio.arg & 0x1ff; 
                    }
                    sdioc->cmd53_sm.data_p = cmd->data;
                    spi_nbytes_transfer(slot, 1);
                }
            }        
        } else {
            /* Continue to poll. */
            spi_nbytes_transfer(slot, 1);
        }
        break;

    case CMD53_STATE_WRITE_DATA_BLOCK:
        if (sdioc->cmd53_sm.prev == CMD53_STATE_WRITE_DATA_BLOCK) {
            /* Check the previous block transfer. */
            if (cmd53_check_data_response_and_busy(rx, t->len) == 0) {
                cmd->status = SDD_CMD_OK;
            } else {
                cmd->status = SDD_CMD_ERR_DAT_OTHER;
                sdio_cmd_complete(slot, cmd);   
                break;
            }            
        } else {
            sdioc->cmd53_sm.prev = CMD53_STATE_WRITE_DATA_BLOCK;
        }

        t->len = 0;
        
        /* Write data block. */
        *tx = 0xff; /* wait 1 byte */
        tx++;
        t->len += 1;

        if (cmd->sdio.arg & CMD53_BLOCK_MODE) {
            if( ( cmd->sdio.arg & 0x1ff ) > 1 )
                *tx = 0xfc; /* start token */
            else
                *tx = 0xfe; /* start token */
            tx++;
        } else {
            *tx = 0xfe;
            tx++;            
        }
        t->len += 1;

        /* Data block. */
        memcpy(tx, sdioc->cmd53_sm.data_p, sdioc->cmd53_sm.block_size); /* data */
        tx += sdioc->cmd53_sm.block_size;
        t->len += sdioc->cmd53_sm.block_size;
        sdioc->cmd53_sm.data_p += sdioc->cmd53_sm.block_size;
        
        /* CRC */
        memset(tx, 0xff, CMD53_CRC_LEN);
        tx += CMD53_CRC_LEN;
        t->len += CMD53_CRC_LEN;

        /* data response */
        memset(tx, 0xff, CMD53_D_RESP_LEN);
        tx += CMD53_D_RESP_LEN;
        t->len += CMD53_D_RESP_LEN;
        
        m->complete    = sdio_spi_cmd53_state_machine;
        m->context     = slot;
        
        ret = spi_async(sdioc->spi, m);
        if (ret < 0) {
            dev_dbg(&sdioc->spi->dev, "%s: failed: ret = %d\n", __FUNCTION__, ret);
        }    
        if (sdioc->cmd53_sm.blocks_remaining > 1) {
            sdioc->cmd53_sm.blocks_remaining--;
        } else {
            sdioc->cmd53_sm.status = CMD53_STATE_DATA_WRITE_COMPLETED;
        }
        break;

    case CMD53_STATE_AFTER_RESPONSE:
        /* Activate timeout. */
         sdioc->cmd53_sm.timeout = jiffies + HZ; /* 1 sec timout. */
        
        /* Read secon byte of response and start to poll. */
        sdioc->cmd53_sm.status = CMD53_STATE_POLL_DATA_READ;
        spi_nbytes_transfer(slot, 1);
        break;
        
    case CMD53_STATE_POLL_DATA_READ:
        val = rx[0];
        if (val == 0xfe) {
            sdioc->cmd53_sm.status = CMD53_STATE_READ_DATA_BLOCK;
            spi_nbytes_transfer(slot, sdioc->cmd53_sm.block_size + CMD53_CRC_LEN);             
        } else {
            if (time_after(jiffies, sdioc->cmd53_sm.timeout)) {
                dev_dbg(&sdioc->spi->dev, "no data\n");
                cmd->status = SDD_CMD_ERR_CMD_TIMEOUT;
                sdio_cmd_complete(slot, cmd);                
            } else {
                /* Continue to poll. */
                spi_nbytes_transfer(slot, 1);
            }
        }
        break;

    case CMD53_STATE_READ_DATA_BLOCK:        
        memcpy(sdioc->cmd53_sm.data_p, rx, sdioc->cmd53_sm.block_size);
        if (sdioc->cmd53_sm.blocks_remaining > 1) {

            sdioc->cmd53_sm.blocks_remaining--;
            sdioc->cmd53_sm.data_p += (sdioc->cmd53_sm.block_size);
            
            /* Activate timeout. */
             sdioc->cmd53_sm.timeout = jiffies + HZ; /* 1 sec timout. */

            /* Poll for the next block. */
            sdioc->cmd53_sm.status = CMD53_STATE_POLL_DATA_READ;
            spi_nbytes_transfer(slot, 1);            
        } else {
            sdio_cmd_complete(slot, cmd); ;
        }
        break;

    case CMD53_STATE_DATA_WRITE_COMPLETED:
        if (cmd53_check_data_response_and_busy(rx, t->len) == 0) {
            cmd->status = SDD_CMD_OK;
            sdio_cmd_complete(slot, cmd);
            break;
        }
        cmd->status = SDD_CMD_ERR_DAT_OTHER;
        sdio_cmd_complete(slot, cmd);   
        break;

    default:
        dev_dbg(&sdioc->spi->dev, "%s State Machine state not defined\n", __FUNCTION__);
    }
}

static int sdio_spi_start_cmd(struct sdio_slot *slot, struct sdio_cmd *cmd)
{
    struct sdio_spi_controller *sdioc = slot->drv_data;
    struct spi_message *m = &sdioc->spi_msg;
    struct spi_transfer *t;
    size_t transfer_len;
    uint8_t *tx;
    int ret = -ENOMEM;

    sdioc->current_cmd = cmd;

    if (cmd->flags & SDD_CMD_FLAG_CSPI) {
        return cspi_start_cmd(slot, cmd);
    }

    transfer_len = SDIO_SPI_CMD_LEN + SDIO_SPI_N_CR_MAX;
    switch (cmd->sdio.cmd) {
    case 0:
        transfer_len += SDIO_SPI_R1_LEN;
        break;
    case 5:
        transfer_len += SDIO_SPI_R4_LEN;
        break;
    case 52:
        transfer_len += SDIO_SPI_R5_LEN;
        break;
    case 53:
        /* Just send the 6 bytes command. */
        transfer_len = SDIO_SPI_CMD_LEN;
        break;
    default:
        dev_err(&sdioc->spi->dev, "CMD%d not supported\n", cmd->sdio.cmd);
        return -EINVAL;
    };
    transfer_len += SDIO_SPI_N_EC_MIN;

    t = list_entry(m->transfers.next, struct spi_transfer, transfer_list);
    tx = (uint8_t *)t->tx_buf;

    /* Add the init sequence to CMD0s. */
    if (cmd->sdio.cmd == 0) {
        memset(tx, 0xff, SDIO_SPI_INIT_SEQ_LEN);
        tx += SDIO_SPI_INIT_SEQ_LEN;
        transfer_len += SDIO_SPI_INIT_SEQ_LEN;
    }

    tx[0] = 0x40 | cmd->sdio.cmd;
    tx[1] = (cmd->sdio.arg >> 24) & 0xff;
    tx[2] = (cmd->sdio.arg >> 16) & 0xff;
    tx[3] = (cmd->sdio.arg >> 8)  & 0xff;
    tx[4] = (cmd->sdio.arg >> 0)  & 0xff;
    tx[5] = 0x01 | 0x94; /* CRC for CMD0, ignored for other commands */
    memset(tx + SDIO_SPI_CMD_LEN, 0xff, transfer_len - SDIO_SPI_CMD_LEN);

    t->len         = transfer_len;
    if (cmd->sdio.cmd == 53) {
        m->complete = sdio_spi_cmd53_state_machine;
        sdioc->cmd53_sm.status =  CMD53_STATE_AFTER_CMD_WRITE; 
    } else {
        m->complete    = sdio_spi_cmd_complete;
    }
    m->context     = slot;

    ret = spi_async(sdioc->spi, m);
    if (ret < 0) {
        dev_dbg(&sdioc->spi->dev, "%s: failed: ret = %d\n", __FUNCTION__, ret);
    }

    return ret;
}

static int sdio_spi_card_power(struct sdio_slot *slot, enum sdio_power pwr)
{
    return 0;
}

static int sdio_spi_card_present(struct sdio_slot *slot)
{
    return 1;
}

/*
 * With edge triggered interrupts there's no need to mask the
 * interrupt signal as a new interrupt will only be triggered when the
 * device asserts the interrupt again after it's cleared.
 */
/* FIXME: This should be in platform data. */

static void sdio_spi_enable_card_int(struct sdio_slot *slot)
{
    struct sdio_spi_controller *sdioc = slot->drv_data;
    struct spi_device *spidev = sdioc->spi;
    struct sdio_spi_plat_data *spipd = spidev->dev.platform_data;

    spipd->enable_int(slot);
}

static void sdio_spi_disable_card_int(struct sdio_slot *slot)
{
}

static irqreturn_t sdio_spi_int_handler(int irq, void *dev_id)
{
    struct spi_device *spi = dev_id;
    struct sdio_slot *slot = dev_get_drvdata(&spi->dev);

    sdio_interrupt(slot);
    
    return IRQ_HANDLED;
}

static int sdio_spi_set_bus_freq(struct sdio_slot *slot, int clk)
{
    struct sdio_spi_controller *sdioc = slot->drv_data;
    
    if( clk == SDD_BUS_FREQ_OFF ||
        clk == SDD_BUS_FREQ_DEFAULT ||
         clk == SDD_BUS_FREQ_IDLE ) {
        /* None of these apply to SPI as the clock will be stopped whenever there is
         * no data
         */
        return 0;
    }
    
    sdioc->spi->max_speed_hz = clk;

    return sdioc->spi->master->setup( sdioc->spi );
}

static int __devinit sdio_spi_probe(struct spi_device *spi)
{
    struct sdio_slot *slot;
    struct sdio_spi_controller *sdioc;
    int ret;

    slot = sdio_slot_alloc(sizeof(struct sdio_spi_controller));
    if (!slot) {
        return -ENOMEM;
    }
    dev_set_drvdata(&spi->dev, slot);

    strcpy(slot->name, spi->dev.bus_id);
    slot->type               = SDD_SLOT_TYPE_SPI_CSPI;
    slot->start_cmd          = sdio_spi_start_cmd;
    slot->card_present       = sdio_spi_card_present;
    slot->card_power         = sdio_spi_card_power;
    slot->enable_card_int    = sdio_spi_enable_card_int;
    slot->set_bus_freq       = sdio_spi_set_bus_freq;
    slot->disable_card_int   = sdio_spi_disable_card_int;
    slot->caps.max_bus_width = 1;
    slot->caps.cspi_mode     = CSPI_MODE_LEN_FIELD_PRESENT;
    if (cspi_big_endian) {
        slot->caps.cspi_mode |= CSPI_MODE_BE_BURST;
    }

    sdioc = slot->drv_data;

    sdioc->spi          = spi;
    sdioc->transfer_buf = (void *)__get_free_page(GFP_KERNEL);
    if (!sdioc->transfer_buf) {
        ret = -ENOMEM;
        goto err;
    }

    /* Initialize a SPI message with a single transfer. */
    spi_message_init(&sdioc->spi_msg);
    sdioc->spi_msg.context       = slot;
    sdioc->spi_trans.tx_buf      = sdioc->transfer_buf;
    sdioc->spi_trans.rx_buf      = sdioc->transfer_buf + PAGE_SIZE/2;
    sdioc->spi_trans.cs_change   = 1;
    sdioc->spi_trans.delay_usecs = 0;
    spi_message_add_tail(&sdioc->spi_trans, &sdioc->spi_msg);

    ret = request_irq(spi->irq, sdio_spi_int_handler, 0, spi->dev.bus_id, spi);
    if (ret) {
        goto err;
    }

    ret = sdio_slot_register(slot);
    if (ret) {
        goto err_slot_reg;
    }

    return 0;

  err_slot_reg:
    free_irq(spi->irq, spi);
  err:
    if (sdioc->transfer_buf)
        free_page((unsigned long)sdioc->transfer_buf);
    sdio_slot_free(slot);
    return ret;
}

static int __devexit sdio_spi_remove(struct spi_device *spi)
{
    struct sdio_slot *slot = dev_get_drvdata(&spi->dev);
    struct sdio_spi_controller *sdioc = slot->drv_data;

    sdio_slot_unregister(slot);
    free_irq(spi->irq, spi);
    free_page((unsigned long)sdioc->transfer_buf);
    sdio_slot_free(slot);

    return 0;
}

static struct spi_driver sdio_spi_driver = {
    .driver = {
        .name	= "slot_spi",
        .bus	= &spi_bus_type,
        .owner	= THIS_MODULE,
    },
    .probe  = sdio_spi_probe,
    .remove = __devexit_p(sdio_spi_remove),
};

static int __init slot_spi_init(void)
{
    return spi_register_driver(&sdio_spi_driver);
}
module_init(slot_spi_init);

static void __exit slot_spi_exit(void)
{
    spi_unregister_driver(&sdio_spi_driver);
}
module_exit(slot_spi_exit);

MODULE_DESCRIPTION("SDIO-SPI and CSPI slot driver");
MODULE_AUTHOR("Cambridge Silicon Radio Ltd.");
MODULE_LICENSE("GPL and additional rights");
