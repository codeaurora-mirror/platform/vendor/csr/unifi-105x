#! /usr/bin/perl -w

=head1 NAME

sdio-log-decode - decode a SDIO event log produced by the sdioemb driver

=head1 SYNOPSIS

sdio-log-decode [OPTION]... [LOG] | --help

=head1 OPTIONS

=over

=item B<-c>, B<--card>=I<CARD>

Decode addresses for I<CARD>. Supported cards are: B<generic> (the default),
B<anastasia>, B<bigfoot>, B<cinderella>, B<dash>, and B<sugarlump>.

=item B<-d>, M<--data>

Show the transferred data (if available).

=back

=head1 DESCRIPTION

B<sdio-log-decode> decodes an event log produced by the sdioemb
driver.  It reads the specified file (or stdin) and outputs the
commands with decoded arguments and responses.

=head1 KEY

  W - CMD52/53 or CSPI write
  R - CMD52/53 or CSPI read
  B - CSPI burst transfer
  Y - CMD53 byte mode transfer
  K - CMD53 block mode transfer
  F - CMD53 fixed address
  I - CMD53 incrementing address
  c - Response CRC error
  t - Response timeout
  C - Data CRC error
  T - Data timeout
  e - Unspecified response error
  E - Unspecified data error

=cut

use strict;
use Getopt::Long qw(:config bundling);
use Pod::Usage;

my %csr_f0_vendor_regs = (
    0xf0 => "CSR_HOST_DEEP_SLEEP",
    0xf1 => "CSR_HOST_INT_CLR",
    0xf2 => "CSR_FROM_HOST_SCRATCH0",
    0xf3 => "CSR_FROM_HOST_SCRATCH1",
    0xf4 => "CSR_TO_HOST_SCRATCH0",
    0xf5 => "CSR_TO_HOST_SCRATCH1",
    0xf6 => "CSR_EXT_IO_ENABLE",
    0xf7 => "CSR_CSPI_MODE",
    0xf8 => "CSR_CSPI_STATUS",
    0xf9 => "CSR_CSPI_PADDING",
);

my %bluetooth_type_a_regs = (
    0x00 => "BT_TYPE_A_RD/TD",
    0x10 => "BT_TYPE_A_RX_PKT_CTRL",
    0x11 => "BT_TYPE_A_TX_PKT_CTRL",
    0x12 => "BT_TYPE_A_RETRY_CTRL",
    0x13 => "BT_TYPE_A_INT",
    0x14 => "BT_TYPE_A_INT_EN",
    0x20 => "BT_TYPE_A_BT_MODE",
);

# Special code to describe an access through one of Bigfoot or
# Pumpkins memory windows.  There are two program memory windows and
# one shared window.
sub print_bigfoot_gw($$)
  {
    my @wind = ("PM1", "PM2", "SH");
    my @proc = ("MAC.", "PHY.", "BOTH.", "BOTH.", "");
    my ($addr, $pages) = @_;
    my ($off, $j) = (0);
    my $w = int($addr / 0x4000);
    my $p = 4;

    foreach $j (@{$pages}) {
        if ($j->{shift} >= 0) {
            if (defined $j->{val}) {
	        $off += $j->{val} << $j->{shift};
	    } else {
		return sprintf("%s[??%04X]", $wind[$w], $addr & 0x3fff);
	    }
        } else {
            if (defined $j->{val}) {
	        $p = $j->{val};
	    }
	}
    }
    my $win;
    if ($w == 2) {
        $win = "SH";
    } else {
        my $hb = $off >> 21;
        if ($hb == 0) {
	    $win = "FLASH";
	    $off &= 0xfffff;
        } elsif ($hb == 1) {
	    $win = $proc[$p] . "P_RAM";
	    $off &= 0xfffff;
        } else {
	    $win = "EXT RAM";
	    $off &= 0x1fffff;
        }
    }
    return sprintf("%s[%06X]", $win, ($addr & 0x3fff) + $off);
  }

# Special code to describe an access through one of Anastasias memory
# windows.  There are thee generic wondows that can be used for
# program memory accesses or for shared memory.  This will probably
# work for Cinderella.
sub print_anas_gw($$)
  {
    my @proc = ("MAC.", "PHY.", "BT.", "BOTH.", "");
    my ($addr, $pages) = @_;
    my ($off, $j) = (0);
    my $w = int($addr / 0x4000);
    my $p = 4;

    foreach $j (@{$pages}) {
        if ($j->{shift} >= 0) {
            if (defined $j->{val}) {
	        $off += $j->{val} << $j->{shift};
	    } else {
	        return sprintf("GW%d[??%04X]", $w + 1, ($addr & 0x3fff));
	    }
        } else {
            if (defined $j->{val}) {
	        $p = $j->{val};
	    }
	}
    }

    $addr = ($addr & 0x3fff) + $off;
    my $page = ($addr >> 22) & 0xf;

    my @gw_area = (
		   "SHARED", "SHARED", "SHARED", "SHARED",
		   "I ROM",  "I ROM",  "unused", "I RAM", # Internal
		   "IO LOG", "IO LOG", "IO LOG", "IO LOG",
		   "A ROM",  "A ROM",  "unused", "A RAM", # Alternative
		  );
    my @gw_bits = (
		   24, 24, 24, 24,
		   22, 22, 22, 22,
		   24, 24, 24, 24,
		   22, 22, 22, 22,
		  );
    return sprintf("GW%d[%-7s %07X]",
		   $w + 1, $gw_area[$page],
		   ((1 << $gw_bits[$page]) - 1) & $addr);
  }

my %registers = (
    'generic' => {
        0 => {
            0x00 => "CCCR_CCCR_SDIO_REVISION",
            0x01 => "CCCR_SD_SPEC_REVISION",
            0x02 => "CCCR_IO_ENABLE",
            0x03 => "CCCR_IO_READY",
            0x04 => "CCCR_INT_ENABLE",
            0x05 => "CCCR_INT_PENDING",
            0x06 => "CCCR_IO_ABORT",
            0x07 => "CCCR_BUS_IFACE_CTRL",
            0x08 => "CCCR_CARD_CAPS",
            0x09 => "CCCR_COMMON_CIS_PTR",
            0x0a => "CCCR_COMMON_CIS_PTR+1",
            0x0b => "CCCR_COMMON_CIS_PTR+2",
            0x0c => "CCCR_BUS_SUSPEND",
            0x0d => "CCCR_FUNCTION_SELECT",
            0x0e => "CCCR_EXEC_FLAGS",
            0x0f => "CCCR_READY_FLAGS",
            0x10 => "CCCR_F0_BLOCK_SIZE",
            0x11 => "CCCR_F0_BLOCK_SIZE+1",
            0x12 => "CCCR_POWER_CTRL",
            0x13 => "CCCR_HIGH_SPEED",
            0x100 => "CCCR_F1_STD_IFACE",
            0x101 => "CCCR_F1_SID_IFACE_EXT",
            0x102 => "CCCR_F1_POWER_SELECT",
            0x109 => "CCCR_F1_CIS_PTR",
            0x10a => "CCCR_F1_CIS_PTR+1",
            0x10b => "CCCR_F1_CIS_PTR+2",
            0x10c => "CCCR_F1_CSA_PTR",
            0x10d => "CCCR_F1_CSA_PTR+1",
            0x10e => "CCCR_F1_CSA_PTR+2",
            0x110 => "CCCR_F1_BLOCK_SIZE",
            0x111 => "CCCR_F1_BLOCK_SIZE+1",
            0x200 => "CCCR_F2_STD_IFACE",
            0x201 => "CCCR_F2_SID_IFACE_EXT",
            0x202 => "CCCR_F2_POWER_SELECT",
            0x209 => "CCCR_F2_CIS_PTR",
            0x20a => "CCCR_F2_CIS_PTR+1",
            0x20b => "CCCR_F2_CIS_PTR+2",
            0x20c => "CCCR_F2_CSA_PTR",
            0x20d => "CCCR_F2_CSA_PTR+1",
            0x20e => "CCCR_F2_CSA_PTR+2",
            0x210 => "CCCR_F2_BLOCK_SIZE",
            0x211 => "CCCR_F2_BLOCK_SIZE+1",
        },
    },
    'sugarlump' => {
        0 => \%csr_f0_vendor_regs,
        1 => \%bluetooth_type_a_regs,
        2 => {
            0xFC24 << 1 => "SDIO_HOST_INT",
            0xFC26 << 1 => "SDIO_MODE",
        },
    },
    'bigfoot' => {
        1 => {
            0x1FD38 => "DBG_EMU_CMD",
            0x1FD20 => "DBG_HOST_PROC_SELECT",
            0x1FD18 => "DBG_HOST_STOP_STATUS",
            0x1FD24 => "DBG_RESET",
            0x1FD02 => "GBL_CHIP_VERSION",
            0x1FFD2 => "XAP_PCH",
            0x1FFD4 => "XAP_PCL",
            0x1FCD6 => "SHARED_MAILBOX0",
            0x1FCD4 => "SHARED_MAILBOX1",
            0x1FCD2 => "SHARED_MAILBOX2",
            0x1FCD0 => "SHARED_MAILBOX3",
            0x1FBD2 => "SHARED_MAILBOX2B",
            0x1FCF6 => "PROG_MEM1_PAGE",
            0x1FCF0 => "PROG_MEM2_PAGE",
            0x1FCFC => "SHARED_MEM_PAGE",
            0x1FCCC => "PCI_INT_EVENT",
            0x1FCCE => "SDIO_HOST_INT",
            0x1FCCA => "SHARED_IO_INTERRUPT",
            windows => [
                {
                    start => 0x0100,
                    end   => 0x4000,
		    pfunc => \&print_bigfoot_gw,
                    page  => [
                        { reg => 0xfe7b * 2,     shift => 12, },
                        { reg => 0xfe7b * 2 + 1, shift => 20, },
                        { reg => 0x1fd20       , shift => -1, },
                    ]
                },
                {
                    start => 0x4000,
                    end   => 0x8000,
		    pfunc => \&print_bigfoot_gw,
                    page  => [
                        { reg => 0xfe78 * 2,     shift => 12, },
                        { reg => 0xfe78 * 2 + 1, shift => 20, },
                        { reg => 0x1fd20       , shift => -1, },
                    ]
                },
                {
                    start => 0x8000,
                    end   => 0xc000,
		    pfunc => \&print_bigfoot_gw,
                    page  => [
                        { reg => 0xfe7e * 2,     shift => 12, },
                        { reg => 0xfe7e * 2 + 1, shift => 20, },
                        { reg => 0x1fd20       , shift => -1, },
                    ]
                },
            ]
        },
    },
    # UniFi Anastasia.  This configuration is for an emulator build,
    # so things might change at any time.  I believe that many other
    # chips will be broadly similar (dash, cinderella, BC7, etc.)
    'anastasia' => {
        0 => \%csr_f0_vendor_regs,
        1 => \%bluetooth_type_a_regs,
        2 => {
            0xf81d * 2 => "DBG_EMU_CMD",
            0xf81e * 2 => "DBG_HOST_PROC_SELECT",
            0xf81f * 2 => "DBG_HOST_STOP_STATUS",
            0xf82f * 2 => "DBG_RESET",
            0xfe81 * 2 => "GBL_CHIP_VERSION",
            0xffe9 * 2 => "XAP_PCH",
            0xffea * 2 => "XAP_PCL",
            0xf84b * 2 => "SHARED_MAILBOX0",
            0xf84c * 2 => "SHARED_MAILBOX1",
            0xf84d * 2 => "SHARED_MAILBOX2",
            0xf84e * 2 => "SHARED_MAILBOX3",
            0xfb94 * 2 => "SHARED_MAILBOX4",
            0xfb95 * 2 => "SHARED_MAILBOX5",
            0xfb96 * 2 => "SHARED_MAILBOX6",
            0xfb97 * 2 => "SHARED_MAILBOX7",
            0xf92f * 2 => "SDIO_HOST_INT",
            0xf8ac * 2 => "MMU_HOST_GW1_CONFIG",
            0xf8ad * 2 => "MMU_HOST_GW2_CONFIG",
            0xf8ae * 2 => "MMU_HOST_GW3_CONFIG",
	    # Windows allow you to define portions of the address map
	    # that are sliding windows into other areas of memory.
	    # The windows on BigFoot and Pumpkin are different from
	    # those on Anastasia.  Anastasia is similar to dash, BC7
	    # and Cinderella.  Each window can point into one of many
	    # different address maps - hence the need for a helper
	    # function to dislpay which area we are pointing to.
            windows => [
                {
                    start => 0x0100,
                    end   => 0x4000,
		    pfunc => \&print_anas_gw,
                    page  => [
                        { reg => 0xf8ac * 2,     shift => 11, },
                        { reg => 0xf8ac * 2 + 1, shift => 19, },
                        { reg => 0xf81e * 2,     shift => -1, },
                    ]
                },
                {
                    start => 0x4000,
                    end   => 0x8000,
		    pfunc => \&print_anas_gw,
                    page  => [
                        { reg => 0xf8ad * 2,     shift => 11, },
                        { reg => 0xf8ad * 2 + 1, shift => 19, },
                        { reg => 0xf81e * 2,     shift => -1, },
                    ]
                },
                {
                    start => 0x8000,
                    end   => 0xc000,
		    pfunc => \&print_anas_gw,
                    page  => [
                        { reg => 0xf8ae * 2,     shift => 11, },
                        { reg => 0xf8ae * 2 + 1, shift => 19, },
                        { reg => 0xf81e * 2,     shift => -1, },
                    ]
                },
            ]
        },
    },
    'dash' => {
        0 => \%csr_f0_vendor_regs,
        1 => \%bluetooth_type_a_regs,
        2 => {
        },
    },
);

my $card = "generic";
my $show_data = 0;

sub print_decoded_cmd($$$$);
sub print_decoded_cspi($$$$$);
sub status_symbol($);
sub print_decoded_addr($$$);
sub print_decoded_int_event($$);

{
    # Other names for chips
    $registers{'pumpkin'} = $registers{'bigfoot'};
    $registers{'cinderella'} = $registers{'anastasia'};

    # The shortened vesions.
    $registers{'bgft'} = $registers{'bigfoot'};
    $registers{'pmkn'} = $registers{'pumpkin'};
    $registers{'anas'} = $registers{'anastasia'};
    $registers{'cind'} = $registers{'cinderella'};

    GetOptions(
        'c|card=s' =>  \$card,
        'd|data' => \$show_data,
        'help' => sub { pod2usage( -verbose => 1 ); }
        ) || pod2usage( -verbose => 0);

    defined $registers{$card} or die "$0: unrecognized card '$card'\n";
    $#ARGV <= 0 or pod2usage( -verbose => 0);

    if ($#ARGV == -1) {
        *LOG = *STDIN;
    } else {
        my $filename = shift;
        open LOG, "<$filename" or die "$filename: $!\n";
    }

    while (my $line = <LOG>) {
        chomp $line;

        # Seq no and time
        if ($line =~ s/^(\s*\d+\[.*\]: )//) {
            print "$1";
        }

        if ($line =~ m/^CMD(\d\d) arg: (.+) stat: (.+) resp: (.+)$/) {
            print_decoded_cmd(int($1), hex($2), hex($3), hex($4));
        } elsif ($line =~ m/^CSPI cmd:(.+) addr: (.+) ...: (.+) status: (.+) resp: (.+)$/) {
            print_decoded_cspi(hex($1), hex($2), hex($3), hex($4), hex($5));
        } elsif($line =~ m/^(card interrupt.*): (.*)$/) {
            print_decoded_int_event($1, hex($2));
        } elsif ($line =~ m/0x[0-9A-F]{4}:/) {
            if (!$show_data) {
                next;
            }
            print "$line";
        } else {
            # Just print unrecognized lines
            print "$line";
        }

        print "\n";
    }
}

sub print_decoded_cmd($$$$)
{
    my ($cmd, $arg, $stat, $resp) = @_;

    my $func = undef;
    my $addr = undef;
    my $data = undef;

    # Command and argument
    printf("CMD%02u %08x", $cmd, $arg);

    # Decode argument for CMD52s and CMD53
    if ($cmd == 52) {
        $func = ($arg & 0x70000000) >> 28;
        $addr = ($arg & 0x07fffe00) >> 9;

        printf(" %s F%d %s%05x",
               ($arg & 0x80000000) ? 'W' : 'R',   # R/W
               ($func),                           # function
               ($arg & 0x08000000) ? "RAW " : "", # RAW
               ($addr));                          # address
        if ($arg & 0x80000000) {
            printf(" %02x", $arg & 0x000000ff);
            $data = $arg & 0x000000ff;
        } else {
            printf(" --");
        }
        # line up responses
        printf("     ");
    } elsif ($cmd == 53) {
        $func = ($arg & 0x70000000) >> 28;
        $addr = ($arg & 0x07fffe00) >> 9;

        printf(" %s F%d %s %s %05x %3d",
               ($arg & 0x80000000) ? 'W' : 'R',       # R/W
               ($func),                               # function
               ($arg & 0x08000000) ? "K" : "Y",       # block/byte
               ($arg & 0x04000000) ? "I" : "F",       # fixed/incr addr
               ($addr),                               # address
               ($arg & 0x000001ff));                  # count
    } else {
        # line up responses
        printf("                   ");
    }

    my $sym = status_symbol($stat);
    if (defined $sym) {
        printf(" %s", $sym);

        # Response if valid
        if ($cmd != 0 && ($stat == 0x00 || $stat & 0x02)) {
            printf(" %08x", $resp);

            # Decode R5 response for CMD52s and CMD53s
            if ($cmd == 52 || $cmd == 53) {
                # Data read, if available
                if ($cmd == 52
                    && (!($arg & 0x80000000)
                        || ($arg & 0x80000000 && ($arg & 0x08000000)))) {
                    printf(" %02x", $resp & 0x0000000ff);
                    $data = $resp & 0x0000000ff;
                } else {
                    printf(" --");
                }

                # Unexpected transfer states: DIS, CMD (during CMD53),
                # TRN (expect during CMD53) and RFU.
                my $state = ($resp & 0x3000) >> 12;
                
                print " RFU" if $state == 0x3;
                print " TRN" if $state == 0x2 && $cmd != 53;
                print " CMD" if $state == 0x01 && $cmd == 53;
                print " DIS" if $state == 0x00;

                # Response flags
                printf("%s%s%s%s%s",
                       ($resp & 0x00008000) ? " CRC_ERROR" : "",
                       ($resp & 0x00004000) ? " ILLEGAL_CMD" : "",
                       ($resp & 0x00000800) ? " ERROR" : "",
                       ($resp & 0x00000200) ? " FUNC_NUM" : "",
                       ($resp & 0x00000100) ? " OUT_OF_RANGE" : "");
            }
        } else {
            # line up decoded addresses
            print("            ");
        }
    } else {
        # line up decoded addresses
        print("              ");
    }

    print_decoded_addr($func, $addr, $data) if defined $addr;
}

sub print_decoded_cspi($$$$$)
{
    my ($cmd, $addr, $len_val, $status, $resp) = @_;

    my $is_read = $cmd & 0x10;
    my $is_burst = $cmd & 0x40;
    my $func = $cmd & 0x0f;
    my $data;

    printf("CSPI %02x %s%s F%d %06x",
           $cmd, $is_read ? "R" : "W", $is_burst ? "B" : " ", $func,
           $addr);
    if ($is_read && !$is_burst) {
        printf(" ----");
    } else {
        printf(" %04x", $len_val);
    }

    my $sym = status_symbol($status);
    if (defined $sym) {
        printf(" %s %02x", $sym, $resp);
        if ($is_read && !$is_burst) {
            printf(" %04x", $len_val);
        } else {
            printf(" ----");
        }
        printf("%s%s%s",
               ($resp & 0x04) ? " FUNC_DISABLED" : "",
               ($resp & 0x02) ? " CLK_STOPPED" : "",
               ($resp & 0x01) ? " ERROR" : "");
    } else {
        # line up decoded addresses
        printf("         ");
    }

    print_decoded_addr($func, $addr, $data);
}

sub status_symbol($)
{
    # See enum sdio_cmd_status in sdio_api.h for the %stat_symbol
    # bits.
    my %stat_symbol = (
        0x00 => " ",
        0x11 => "c",
        0x21 => "t",
        0x41 => "e",
        0x12 => "C",
        0x22 => "T",
        0x42 => "E",
    );
    my $stat = shift;

    my $sym = $stat_symbol{$stat};
    if (!defined $sym && $stat != 0xff) {
        $sym = "?"; # invalid status
    }
    return $sym;
}

sub print_decoded_addr($$$)
{
    my ($func, $addr, $data) = @_;
    my $reg = undef;
    my ($i, $j);

    $reg = $registers{$card}{$func}{$addr};

    if (!defined $reg && $addr & 0x01) {
        $reg = $registers{$card}{$func}{$addr & ~1};
        if (defined $reg) {
            $reg .= "+1";
        }
    }

    if (!defined $reg && $func == 0) {
        $reg =  $registers{'generic'}{0}{$addr};
    }

    for $i (@{$registers{$card}{$func}{windows}}) {
	if (defined $data) {
	    # Snoop on data.
	    foreach $j (@{$i->{page}}) {
	        if ($addr == $j->{reg}) {
		    $j->{val} = $data;
                }
            }
	}
	if ($addr >= $i->{start} && $addr < $i->{end}) {
	    $reg = $i->{pfunc}->($addr, $i->{page});
	}
    }

    if (defined $reg) {
        print " $reg";
    }
}

sub print_decoded_int_event($$)
{
    my ($text, $funcs) = @_;

    print "$text:";

    for (my $f = 0; $f < 8; $f++) {
        if ($funcs & (1 << $f)) {
            printf(" F%d", $f);
        }
    }
    if ($funcs & (1 << 8)) {
        printf(" UIF");
    }
}
