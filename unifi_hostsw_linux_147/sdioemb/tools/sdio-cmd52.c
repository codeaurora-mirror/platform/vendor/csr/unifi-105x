/*
 * Utility to read/write SDIO registers with CMD52s.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <stdio.h>
#include <stdlib.h>

#include <sdioemb/libsdio.h>

int main(int argc, char *argv[])
{
    sdio_uif_t sdev;
    int func;
    uint32_t addr;
    uint8_t data;

    if (argc < 4) {
        fprintf(stderr, "Usage: %s <sdio device> <func> <addr> [<value>]\n",
                argv[0]);
        exit(1);
    }

    sdev = sdio_open(argv[1], NULL, NULL);
    if (!sdev) {
        perror("sdio_open");
        exit(1);
    }

    func = strtoul(argv[2], NULL, 0);
    addr = strtoul(argv[3], NULL, 0);

    if (func > sdio_num_functions(sdev)) {
        fprintf(stderr, "%s: function %d isn't present\n", argv[0], func);
        exit(1);
    }

    if (argc == 4) {
        if (sdio_read8(sdev, func, addr, &data) < 0) {
            perror("sdio_read8");
            exit(1);
        }
        printf("F%d 0x%05x -> 0x%02x\n", func, addr, data);
    }

    if (argc == 5) {
        data = strtoul(argv[4], NULL, 0);
        if (sdio_write8(sdev, func, addr, data) < 0) {
            perror("sdio_write8");
            exit(1);
        }
        printf("F%d 0x%05x <- 0x%02x\n", func, addr, data);
    }

    return 0;
}
