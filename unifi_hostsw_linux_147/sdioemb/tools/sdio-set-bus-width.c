/*
 * Utility to set the SD bus width to 1 or 4 bits.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <stdio.h>
#include <stdlib.h>

#include <sdioemb/libsdio.h>

int main(int argc, char *argv[])
{
    sdio_uif_t sdev;
    int width;

    if (argc < 3) {
        fprintf(stderr, "Usage: %s <sdio device> <bus width>\n", argv[0]);
        exit(1);
    }

    sdev = sdio_open(argv[1], NULL, NULL);
    if (!sdev) {
        perror("sdio_open");
        exit(1);
    }

    width = atoi(argv[2]);
    if (width != 1 && width != 4) {
        fprintf(stderr, "%s: bus width must be 1 or 4\n", argv[0]);
        exit(1);
    }
    sdio_set_bus_width(sdev, width);

    return 0;
}
