/*
 * Linux interface to the SDIO event log.
 *
 * The log is presented as text in /proc/driver/slot%d_event_log.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

#include <sdioemb/cspi.h>
#include "sdio_layer.h"

#ifdef SDD_DEBUG_EVENT_LOG

static void *sdio_event_log_start(struct seq_file *s, loff_t *pos)
{
    struct sdio_slot *slot = s->private;

    return sdio_event_log_get_entry(slot, *pos);
}

static void *sdio_event_log_next(struct seq_file *s, void *v, loff_t *pos)
{
    struct sdio_slot *slot = s->private;

    (*pos)++;
    return sdio_event_log_get_entry(slot, *pos);
}

static void sdio_event_log_stop(struct seq_file *s, void *v)
{
}

static int sdio_event_log_show(struct seq_file *s, void *v)
{
    struct sdio_event_entry *e = v;

    seq_printf(s, "%6u[%7lu.%03lu]: ",
               e->seq_num, e->time_ms / 1000, e->time_ms % 1000);

    switch (e->type) {
    case SDD_EVENT_CMD:
        if (e->cmd.flags & SDD_CMD_FLAG_CSPI) {
            seq_printf(s, "CSPI cmd:%02x addr: %06x %s: %04x status: %02x resp: %02x\n",
                       e->cmd.cspi.cmd, e->cmd.cspi.addr,
                       e->cmd.cspi.cmd & CSPI_BURST ? "len" : "val", e->cmd.cspi.val,
                       e->cmd.status, e->cmd.cspi.response);
        } else {
            seq_printf(s, "CMD%02u arg: %08x stat: %02x resp: %08x\n",
                       e->cmd.sdio.cmd, e->cmd.sdio.arg, e->cmd.status, e->cmd.sdio.response.r1);
        }
#if SDD_DEBUG_DATA_BUFFER_LEN > 0
        if (e->cmd.data.length != 0) {
            unsigned int i;

            for (i = 0; i < e->cmd.data.length; i++) {
                if ((i % 16) == 0) {
                    seq_printf(s, "                     0x%04X:", i);
                }

                seq_printf(s, " %02X", sdio_event_log_get_data(e, i));

                if ((i % 16) == 15) {
                    seq_putc(s, '\n');
                }
            }
            if ((i % 16) != 0) {
                seq_putc(s, '\n');
            }
        }
#endif
        break;
    case SDD_EVENT_CARD_INT:
        seq_printf(s, "card interrupt\n");
        break;
    case SDD_EVENT_INT_MASKED:
        seq_printf(s, "card interrupt masked: %02x\n", e->functions);
        break;
    case SDD_EVENT_INT_UNMASKED:
        seq_printf(s, "card interrupt unmasked: %02x\n", e->functions);
        break;
    case SDD_EVENT_POWER_ON:
        seq_printf(s, "power on\n");
        break;
    case SDD_EVENT_POWER_OFF:
        seq_printf(s, "power off\n");
        break;
    case SDD_EVENT_RESET:
        seq_printf(s, "hard reset\n");
        break;
    case SDD_EVENT_CLOCK_FREQ:
        seq_printf(s, "clock frequency request: %d kHz\n", e->val / 1000);
        break;
    }

    return 0;
}

static struct seq_operations sdio_event_log_seq_ops = {
    .start = sdio_event_log_start,
    .next  = sdio_event_log_next,
    .stop  = sdio_event_log_stop,
    .show  = sdio_event_log_show,
};

static int sdio_event_log_proc_open(struct inode *inode, struct file *file)
{
    int ret;

    ret = seq_open(file, &sdio_event_log_seq_ops);
    if (!ret) {
        ((struct seq_file *)file->private_data)->private = PDE(inode)->data;
    }
    return ret;
}

static struct file_operations sdio_event_log_proc_ops = {
    .owner = THIS_MODULE,
    .open  = sdio_event_log_proc_open,
    .read  = seq_read,
    .llseek = seq_lseek,
    .release = seq_release,
};

void sdio_os_event_log_init(struct sdio_slot *slot)
{
    struct proc_dir_entry *proc_entry;
    char path[32];

    sprintf(path, "driver/slot%d_event_log", slot->priv->id);
    proc_entry = create_proc_entry(path, 0444, NULL);
    if (proc_entry) {
        proc_entry->data = slot;
        proc_entry->proc_fops = &sdio_event_log_proc_ops;
    }
}

void sdio_os_event_log_deinit(struct sdio_slot *slot)
{
    char path[32];

    sprintf(path, "driver/slot%d_event_log", slot->priv->id);
    remove_proc_entry(path, NULL);
}

#endif /* #ifdef SDD_DEBUG_EVENT_LOG */
